<?php
require_once('controller.php');
require_once($conf['DOC_ROOT'].'models/ad_model.php');
require_once($conf['DOC_ROOT'].'views/main.php');

/**
 * The main controller class for BestSiteAd, which manages the site and
 * determines what the users will see.
 * Authors: Nicolas Seto, Dora Do
 */
class MainController implements Controller {
    private $model;
    private $view;
    private $ads;
    
    function __construct() {
        $this->model = new AdModel();
    }
    
    /**
     * Process the request based on request.
     */
    public function process() {
        /* If POST request delAd is sent, the user wants to delete an ad. */
        if (isset($_POST['delAd'])) {
            $values = explode(" ", $_POST['delAd']);
            $this->model->deleteAd((int) $values[2]);
            unset($_POST['delAd']);
        /* If title is set, a submission has been made. */
        } else if (isset($_POST['title'])) {
            $data = array($_POST['title'], $_POST['url'], $_POST['desc']);
            $this->model->addAd($data);
            unset($_POST['title']);
            unset($_POST['url']);
            unset($_POST['desc']);
        /* If reset is set, a user hit the Reset Counters button. */
        } else if (isset($_POST['reset'])) {
            $this->model->resetClicks();
            unset($_POST['reset']);
        }
        $ads = $this->model->getAds();
        
        $view = new MainView($ads);
        $view->display();
    }
}
?>