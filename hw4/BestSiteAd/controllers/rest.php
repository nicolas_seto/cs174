<?php
require_once('controller.php');
require_once($conf['DOC_ROOT'].'models/ad_model.php');
require_once($conf['DOC_ROOT'].'views/rest.php');

/**
 * The REST controller class for BestSiteAd, which manages the REST calls.
 * Authors: Nicolas Seto, Dora Do
 */
class RestController implements Controller {
    private $method;
    private $model;
    private $ad;
    private $view;
    
    function __construct($method) {
        $this->method = $method;
        $this->model = new AdModel();
    }
    
    /**
     * Process the request based on request.
     */
    public function process() {
        global $conf;
        $xml = "";
        $json = "";
        if ($this->method == 'get-ad') {
            $this->ad = $this->model->getAdRandom();
            /* User requests ad in xml */
            if ($_GET['format'] == 'xml') {
                $xml .= <<<EOF
<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE ad SYSTEM "{$conf['baseURL']}xml/ad.dtd">
<ad id="{$this->ad['AdID']}">
    <title>{$this->ad['Title']}</title>
    <url>{$this->ad['URL']}</url>
    <description>{$this->ad['Description']}</description>
</ad>
EOF;
                $this->view = new RestView($xml, 0);
            /* User requests ad in json */
            } else {
                $row = array(
                    "title" => $this->ad['Title'],
                    "url" => $this->ad['URL'],
                    "description" => $this->ad['Description']
                    );
                $json = json_encode($row);
                $this->view = new RestView($json, 1);
            }
            $this->view->display();
            
        } else if ($this->method == 'increment-choice') {
            $this->model->incrementAd($_GET['id']);
        } else if ($this->method == 'increment-vulnerable') {
            $this->model->incrementAdVulnerable($_GET['id']);
        }
    }
}
?>