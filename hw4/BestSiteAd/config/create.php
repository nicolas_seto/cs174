<?php

require_once('config.php');

$mysqli = 
    mysqli_connect($conf['db_host'], $conf['db_user'], $conf['db_pass']);
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') ' . 
        mysqli_connect_error());
}

echo "Successfully connected to MySQL...\n";

/* Create Ads database */
$query_create_database = "CREATE DATABASE IF NOT EXISTS {$conf['db_name']};";
if (mysqli_query($mysqli, $query_create_database)) {
    echo "Database {$conf['db_name']} created successfully...\n";
} else {
    echo "Error creating database: " . mysqli_error($mysqli) . '\n';
}

/* Change to Ads database */
mysqli_select_db($mysqli, $conf['db_name']);

/* Multi-line query for creating table populated with advertisements. */
$create_tables = <<<EOF
DROP TABLE IF EXISTS Advertisements;
CREATE TABLE IF NOT EXISTS Advertisements
(AdID INT UNSIGNED NOT NULL AUTO_INCREMENT 
PRIMARY KEY, Title VARCHAR(30) NOT NULL, URL VARCHAR(255) NOT NULL,
Description VARCHAR(255) NOT NULL, Clicks INT NOT NULL DEFAULT 0);
DROP TABLE IF EXISTS NewsStories;
CREATE TABLE IF NOT EXISTS NewsStories
(NewsID INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
Title VARCHAR(30) NOT NULL, URL VARCHAR(255) NOT NULL,
Description VARCHAR(255) NOT NULL);
EOF;

if (mysqli_multi_query($mysqli, $create_tables)) {
    echo "Successfully created tables...\n";
} else {
    echo "Error creating tables:" . mysqli_error($mysqli) . "\n";
}

/* Get rid of results of query */
while (mysqli_more_results($mysqli) && mysqli_next_result($mysqli));

/* Writing out initial ads. */
$title = mysqli_real_escape_string($mysqli, "News Stories");
$url = mysqli_real_escape_string($mysqli, "http://news.yahoo.com");
$description = mysqli_real_escape_string($mysqli, 
"This tuple represents news stories.");

$query = "SET SESSION sql_mode='NO_AUTO_VALUE_ON_ZERO';";
$result = mysqli_query($mysqli, $query);
$query = "INSERT INTO Advertisements(AdID, Title, URL, Description) 
    VALUES(0, '{$title}', '{$url}', '{$description}');";
$result = mysqli_query($mysqli, $query);

$title = mysqli_real_escape_string($mysqli, "3D Gaming Device!");
$url = mysqli_real_escape_string($mysqli, 
"http://www.nintendo.com/3ds/features/#section-3dsxl");
$description = mysqli_real_escape_string($mysqli, 
"Have you ever wondered what it's like to play a game on the go and in 3D?".
" Also, are you tired of squinting your eyes at those tiny screens? Well".
", look no further! The 3DS is here!");

$query = "INSERT INTO Advertisements(Title, URL, Description) 
    VALUES('{$title}', '{$url}', '{$description}');";
$result = mysqli_query($mysqli, $query);

$title = mysqli_real_escape_string($mysqli, "DS 3.0");
$url = mysqli_real_escape_string($mysqli, 
"http://www.nintendo.com/3ds/features/#/3d-screen");
$description = mysqli_real_escape_string($mysqli, 
"The 3DS XL has better graphics and 3D capabilities that bring you into".
" an adventure you have never experienced before. With a 90% bigger screen".
" than the Nintendo 3DS, you can view 90% more 3D!");

$query = "INSERT INTO Advertisements(Title, URL, Description) 
    VALUES('{$title}', '{$url}', '{$description}');";
$result = mysqli_query($mysqli, $query);

$title = mysqli_real_escape_string($mysqli, "It's As If You Can Touch It!");
$url = mysqli_real_escape_string($mysqli,
"http://www.nintendo.com/3ds/features/#/3d-camera");
$description = mysqli_real_escape_string($mysqli,
"Why touch yourself when you can touch your Mii character! With the ".
"3D Camera that is integrated in the 3DS XL, you can create your very own".
" Mii characters as well as customize them.");

$query = "INSERT INTO Advertisements(Title, URL, Description) 
    VALUES('{$title}', '{$url}', '{$description}');";
$result = mysqli_query($mysqli, $query);

/* Writing out default news stories. */
$newsTitle = mysqli_real_escape_string($mysqli, "Black Friday Countdown");
$newsUrl = mysqli_real_escape_string($mysqli, "http://www.mercurynews.com".
"/business/ci_24526498/black-friday-sales-move-thanksgiving");
$newsInfo = mysqli_real_escape_string($mysqli, "As the countdown begins, ".
"we're waiting for the day that Friday becomes pitch black, ".
"and we can't see anything at all!");

$query = "INSERT INTO NewsStories(Title, URL, Description) 
    VALUES('{$newsTitle}', '{$newsUrl}', '{$newsInfo}');";
$result = mysqli_query($mysqli, $query);

$newsTitle = mysqli_real_escape_string($mysqli, "PlayStation 4 vs. Xbox One");
$newsUrl = mysqli_real_escape_string($mysqli, "http://www.theonion.com/".
"articles/playstation-4-vs-xbox-one,34568/");
$newsInfo = mysqli_real_escape_string($mysqli, "Sony and Microsoft are ".
"launching their hotly anticipated next-generation video gaming ".
"consoles this month, with the PlayStation 4 going on sale on Nov. 15 and ".
"the Xbox One arriving on shelves one week later.");

$query = "INSERT INTO NewsStories(Title, URL, Description) 
    VALUES('{$newsTitle}', '{$newsUrl}', '{$newsInfo}');";
$result = mysqli_query($mysqli, $query);

$newsTitle = mysqli_real_escape_string($mysqli, "Teens Becoming Shorter");
$newsUrl = mysqli_real_escape_string($mysqli, "http://www.theonion.com".
"/articles/it-almost-as-if-rite-aid-cashier-doesnt-care-about,34562/");
$newsInfo = mysqli_real_escape_string($mysqli, "A new study reveals ".
"that children have been growing less and less every year. It's expected ".
"in 300 years, a teenager will be the size of a pea.");

$query = "INSERT INTO NewsStories(Title, URL, Description) 
    VALUES('{$newsTitle}', '{$newsUrl}', '{$newsInfo}');";
$result = mysqli_query($mysqli, $query);

$newsTitle = mysqli_real_escape_string($mysqli, "Driving 100MPH");
$newsUrl = mysqli_real_escape_string($mysqli, "http://www.theonion.com/".
"articles/breaking-drunk-teen-going-100-mph-down-slick-highw,34313/");
$newsInfo = mysqli_real_escape_string($mysqli, "Sources say that if you ".
"drive 100+ MPH, you are pretty much invincible.");

$query = "INSERT INTO NewsStories(Title, URL, Description) 
    VALUES('{$newsTitle}', '{$newsUrl}', '{$newsInfo}');";
$result = mysqli_query($mysqli, $query);

$newsTitle = mysqli_real_escape_string($mysqli, "Middle East Conflict ".
"Remains");
$newsUrl = mysqli_real_escape_string($mysqli, "http://www.theonion.com/".
"articles/breaking-middle-east-conflict-not-solved-today,33571/");
$newsInfo = mysqli_real_escape_string($mysqli, "According to late-breaking ".
"reports emerging from Damascus, Gaza, Baghdad and elsewhere across ".
"the region, the deadly, generations-long conflict in the Middle East ".
"was not resolved today.");

$query = "INSERT INTO NewsStories(Title, URL, Description) 
    VALUES('{$newsTitle}', '{$newsUrl}', '{$newsInfo}');";
$result = mysqli_query($mysqli, $query);

$newsTitle = mysqli_real_escape_string($mysqli, "GOP vs. Obamacare");
$newsUrl = mysqli_real_escape_string($mysqli, "http://www.theonion.com/art".
"icles/gop-announces-plan-to-go-after-obamacare,34300/");
$newsInfo = mysqli_real_escape_string($mysqli, "WASHINGTON—In a strategic ".
"announcement that has reportedly left Beltway observers both shocked ".
"and stunned, Republican lawmakers revealed Monday that they were ".
"planning to go after Obamacare.");

$query = "INSERT INTO NewsStories(Title, URL, Description) 
    VALUES('{$newsTitle}', '{$newsUrl}', '{$newsInfo}');";
$result = mysqli_query($mysqli, $query);

$newsTitle = mysqli_real_escape_string($mysqli, "Fire-Based Life");
$newsUrl = mysqli_real_escape_string($mysqli, "http://www.theonion.com/".
"articles/scientists-theorize-sun-could-support-firebased-li,34559/");
$newsInfo = mysqli_real_escape_string($mysqli, "WASHINGTON—In an announcem".
"ent that could forever change the way scientists study the hydrogen-based ".
"star, NASA researchers published a comprehensive study today theorizing ".
"that the sun may be capable of supporting fire-based lifeforms.");

$query = "INSERT INTO NewsStories(Title, URL, Description) 
    VALUES('{$newsTitle}', '{$newsUrl}', '{$newsInfo}');";
$result = mysqli_query($mysqli, $query);

$newsTitle = mysqli_real_escape_string($mysqli, "The PS4");
$newsUrl = mysqli_real_escape_string($mysqli, "http://games.yahoo.com/news/".
"sony-exec-details-six-years-took-create-ps4-175046671.html");
$newsInfo = mysqli_real_escape_string($mysqli, "Sony exec details the ".
"six years of work it took to create the PS4");

$query = "INSERT INTO NewsStories(Title, URL, Description) 
    VALUES('{$newsTitle}', '{$newsUrl}', '{$newsInfo}');";
$result = mysqli_query($mysqli, $query);

$newsTitle = mysqli_real_escape_string($mysqli, "Aftermath of Illinois ".
"Tornado");
$newsUrl = mysqli_real_escape_string($mysqli, "http://news.yahoo.com/fast".
"-moving-storm-kills-five-tornadoes-rip-u-011603809.html");
$newsInfo = mysqli_real_escape_string($mysqli, "WASHINGTON, Illinois ".
"(Reuters) - Some residents of Washington, Illinois, picked through the ".
"remains of their tornado-flattened homes on Monday, recovering what they ".
"could a day after a series of twisters pounded the Midwest, killing six ".
"people in Illinois.");

$query = "INSERT INTO NewsStories(Title, URL, Description) 
    VALUES('{$newsTitle}', '{$newsUrl}', '{$newsInfo}');";
$result = mysqli_query($mysqli, $query);

$newsTitle = mysqli_real_escape_string($mysqli, "Wedding Rings for Game".
" Tickets");
$newsUrl = mysqli_real_escape_string($mysqli, "http://news.yahoo.com/wedding".
"-rings-swapped-chiefs-tickets-231158234.html");
$newsInfo = mysqli_real_escape_string($mysqli, "KANSAS CITY, Mo. (AP) — A ".
"longtime Kansas City Chiefs fan says he swapped six game tickets for a ".
"wedding ring set advertised on Craigslist.");

$query = "INSERT INTO NewsStories(Title, URL, Description) 
    VALUES('{$newsTitle}', '{$newsUrl}', '{$newsInfo}');";
$result = mysqli_query($mysqli, $query);

mysqli_close($mysqli);
?>