<?php
session_start();
error_reporting(-1);
/*Authors: Nicolas Seto, Dora Do*/
require_once('config/config.php');

/* By default, controller is main */
$c = 'main';

/* If controller is specified, use it */
if (isset($_GET['c'])) {
    $c = $_GET['c'];
}
/* If URI follows the following format, it is possible that a REST call
 * is being made.
 */
try {
$url = explode("/index.php/", $_SERVER['REQUEST_URI']);
if (isset($url[1])) {
    $method = explode("/", $url[1]);
    if ($method[0] == 'get-ad' || $method[0] == 'increment-choice' ||
        $method[0] == 'increment-vulnerable') {
            $c = 'rest';
        }
    }
} catch (Exception $e) {}

/* Include code for specified controller */
require_once('controllers/' . $c . '.php');

if ($c == 'main') {
    $controller = new MainController();
} else if ($c == 'rest') {
    $controller = new RestController($method[0]);
} else {
    echo 'Invalid controller specified';
}

$controller->process();
?>