<?php
/** 
 * The base view for BestSiteAd.
 * Authors: Nicolas Seto, Dora Do
 */
interface View {
    /** 
     * Display relevant advertisement information.
     */
    function display();
}
?>