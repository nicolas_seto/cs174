<?php
require_once('view.php');

/**
 * The REST view that retrieves an ad in xml or json.
 */
class RestView implements View {
    private $xml = null;
    private $json = null;
    
    function __construct($data, $identifier) {
        /* If identifier is 1, data is json */
        if ($identifier) {
            $this->json = $data;
        /* If identifier is 0, data is in xml */
        } else {
            $this->xml = $data;
        }
    }
    
    /**
     * Display advertisement in xml or json.
     */
    public function display() {
        global $conf;
        if ($this->xml != null) {
            header('Content-Type: text/xml');
            print_r($this->xml);
        } else {
            header('Content-type: application/json');
            echo "$this->json";
        }
        
    }
}
?>