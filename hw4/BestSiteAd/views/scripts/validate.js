function validateText() {
    var title = document.getElementById("title").value.length;
    
    if (title == 0 || title > 30) {
        return false;
    }
    
    var url = document.getElementById("url").value.length;
    if (url == 0 || url > 255) {
        return false;
    }
    
    var description = document.getElementById("desc").value.length;
    if (description == 0 || description > 255) {'
        return false;
    }
    
    return true;
}

function displayResult()
{
    var title = document.getElementById("title").value.length;
    
    if (title == 0 || title > 30) {
        alert("Title cannot be left empty or greater than 30 characters.\n" +
            "Title length is currently " + titleLength + " characters.\n");
    }
    
    var url = document.getElementById("url").value.length;
    if (url == 0 || url > 255) {
        alert("URL cannot be left empty or greater than 255 characters.\n" +
            "Url length is currently " + urlLength + " characters.\n");
    }
    
    var description = document.getElementById("desc").value.length;
    if (description == 0 || description > 255) {'
        alert("Description cannot be left empty or greater than 255 " +
            "characters.\nDescription length is currently " + descLength +
            " characters.");
    }
}
