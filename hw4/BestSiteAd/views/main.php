<?php
require_once('view.php');

/**
 * The main view that displays the click totals and submission form.
 */
class MainView implements View {
    private $submission;
    
    function __construct($data) {
        $this->submission = $data;
    }
    
    /**
     * Display counters for all the advertisements and a form for submission.
     */
    public function display() {
        global $conf;
        $ads = $this->listAds();
        
        $html_code = <<<EOF
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>BestSiteAd</title>
        <script src="{$conf['baseURL']}views/scripts/validate.js"></script>
    </head>
    <body>
        <table border="1">
        <tr>
            <th>Ad Title</th>
            <th>Clicks / Hits</th>
            <th>Delete Ad?</th>
        </tr>
{$ads}
        </table>
        <br />
        <br />
        <form method="POST" action="{$conf['baseURL']}">
            <input type="submit" name="reset" value="Reset Counters"/>
        </form>
        <br />
        <br />
        <h3>Add an advertisement</h3>
        <form method="POST" action="{$conf['baseURL']}"
        onsubmit="return validateText();">
            <br />
            Ad Title: <input type="text" name="title" id="title"
            maxlength="30" placeholder="Enter your ad's title" />
            <br />
            URL: <input type="text" name="url" id="url" maxlength="255"
            placeholder="The URL of where to find more info on the product" />
            <br />
            Description: <br /><textarea name="desc" id="desc" rows="6" 
            cols="50" placeholder="Write your advertisement here"></textarea>
            <br />
            <input type="submit" value="Submit" onclick="displayResult()"/>
        </form>
    </body>
</html>
EOF;
        echo $html_code;
    }
    
    private function listAds() {
        global $conf;
        $string = "";
        
        for ($i = 0; $i < count($this->submission); $i++) {
            $string .= <<<EOF
        <tr>
            <td>{$this->submission[$i]['Title']}</td>
            <td>{$this->submission[$i]['Clicks']}</td>

EOF;
            if ($i == 0) {
                $string .= <<<EOF
        </tr>

EOF;
            } else {
                $string .= <<<EOF
            <td>
                <form method="POST" action="{$conf['baseURL']}">
                    <input type="submit" name="delAd" 
                    value="Delete Ad {$this->submission[$i]['AdID']}" />
                </form>
            </td>
        </tr>

EOF;
            }
        }
        
        return $string;
    }
}
?>