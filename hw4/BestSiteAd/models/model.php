﻿<?php
/**
 *  The base model for BestSiteAd. Has ability to retrieve ads, increment the
 *  number of clicks or hits, reset the number of clicks, delete ads, or add
 *  ads.
 *  Authors: Nicolas Seto, Dora Do
 */
interface Model {
    /**
     * Retrieves all the ads and its relevant information.
     */
    function getAds();
    
    /**
     * Retrieves the specific information for a certain ad.
     */
    function getAd($adID);
    
    /**
     * Increment the clicks for a certain ad.
     */
    function incrementAd($adID);
    
    /**
     * Increment the clicks for a certain ad (vulnerable).
     */
    function incrementAdVulnerable($adID);
    
    /**
     * Resets the number of clicks for all ads.
     */
    function resetClicks();
    
    /**
     * Deletes a specified ad.
     */
    function deleteAd($adID);
    
    /**
     * Add an ad.
     */
    function addAd($data);
}
?>