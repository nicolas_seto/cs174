<?php
require_once('model.php');
/**
 *  The base model for BestSiteAd. Has ability to retrieve ads, increment the
 *  number of clicks or hits, reset the number of clicks, delete ads, or add
 *  ads.
 *  Authors: Nicolas Seto, Dora Do
 */
class AdModel implements Model {
    
    function __construct() {
    
    }
    
    /**
     * Retrieves all the ads and its relevant information.
     */
    public function getAds() {
        global $conf;
        $con = mysqli_connect($conf['db_host'], $conf['db_user'], 
               $conf['db_pass'], $conf['db_name']);
        $query = "SELECT * FROM Advertisements;";
        $result = mysqli_query($con, $query);
        while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
            $rows[] = $row;
        }
        mysqli_close($con);
        return $rows;
    }
    
    /**
     * Retrieves the specific information for a certain ad.
     */
    public function getAd($adID) {
        global $conf;
        $con = mysqli_connect($conf['db_host'], $conf['db_user'], 
               $conf['db_pass'], $conf['db_name']);
        $query = mysqli_real_escape_string($con, 
            "SELECT * FROM Advertisements WHERE AdID={$adID};");
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        mysqli_close($con);
        return $row;
    }
    
    /**
     * Retrieves a random ad.
     */
    public function getAdRandom() {
        global $conf;
        $con = mysqli_connect($conf['db_host'], $conf['db_user'], 
               $conf['db_pass'], $conf['db_name']);
        $query = "SELECT COUNT(*) AS Num FROM Advertisements;";
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        /* Compute random value between 1 and total number of ads*/
        $random = rand(1, $row['Num'] - 1);
        $query = "SELECT * FROM Advertisements WHERE AdID={$random};";
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        mysqli_close($con);
        return $row;
    }
    
    /**
     * Increment the clicks for a certain ad.
     */
    public function incrementAd($adID) {
        global $conf;
        $con = mysqli_connect($conf['db_host'], $conf['db_user'], 
               $conf['db_pass'], $conf['db_name']);
        $query = mysqli_real_escape_string($con, 
        "SELECT Clicks FROM Advertisements WHERE AdID={$adID};");
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $newClicks = $row['Clicks'] + 1;
        $query = "UPDATE Advertisements SET Clicks={$newClicks} WHERE
        AdID={$adID};";
        mysqli_query($con, $query);
        mysqli_close($con);
    }
    
    /**
     * Increment the clicks for a certain ad (vulnerable).
     */
    public function incrementAdVulnerable($adID) {
        global $conf;
        $con = mysqli_connect($conf['db_host'], $conf['db_user'], 
               $conf['db_pass'], $conf['db_name']);
        $query = "SELECT Clicks FROM Advertisements WHERE AdID={$adID};";
        $result = mysqli_multi_query($con, $query);
        while (mysqli_more_results($con) && mysqli_next_result($con));
        mysqli_close($con);
    }
    
    /**
     * Resets the number of clicks for all ads.
     */
    public function resetClicks() {
        global $conf;
        $con = mysqli_connect($conf['db_host'], $conf['db_user'], 
               $conf['db_pass'], $conf['db_name']);
        $query = "UPDATE Advertisements SET Clicks=0 WHERE Clicks<>0;";
        mysqli_query($con, $query);
        mysqli_close($con);
    }
        
    /**
     * Deletes a specified ad.
     */
    public function deleteAd($adID) {
        global $conf;
        $con = mysqli_connect($conf['db_host'], $conf['db_user'], 
               $conf['db_pass'], $conf['db_name']);
        $query = mysqli_real_escape_string($con, 
            "DELETE FROM Advertisements WHERE AdID={$adID};");
        mysqli_query($con, $query);
        mysqli_close($con);
    }
    
    /**
     * Add an ad.
     */
    public function addAd($data) {
        global $conf;
        $con = mysqli_connect($conf['db_host'], $conf['db_user'], 
               $conf['db_pass'], $conf['db_name']);
        $query = mysqli_real_escape_string($con, 
            "INSERT INTO Advertisements(Title, URL, Description)
            VALUES(\"{$data[0]}\", \"{$data[1]}\", \"{$data[2]}\");");
        mysqli_query($con, $query);
        mysqli_close($con);
    }
}
?>