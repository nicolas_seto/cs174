<?php
require_once('controller.php');
require_once($conf['DOC_ROOT'].'models/news_model.php');
require_once($conf['DOC_ROOT'].'views/main.php');

/**
 * The main controller class for SiteTest, which manages the site and
 * determines what the users will see.
 * Authors: Nicolas Seto, Dora Do
 */
class ProxyController implements Controller {
    /**
     * Process the request based on request.
     */
    public function process() {
        global $conf;
        $base = $conf['baseSiteURL'].'index.php/';
        $path = "";
        
        /* REST call for get-ad is made */
        if ($_GET['m'] == 'get-ad') {
            $path = "{$_GET['m']}/?format={$conf['format']}";
            $url = $base.$path;
            $session = curl_init($url);
            curl_setopt($session, CURLOPT_HEADER, false);
            curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
            $data = curl_exec($session);
            /* If we retrieve data in xml */
            if ($conf['format'] == 'xml') {
                header("Content-Type: text/xml");
            /* We retrieve data in json */
            } else {
                header("Content-Type: application/json");
            }
            
            echo $data;
        /* REST call for increment-choice is made */
        } else {
            $path = "{$_GET['m']}/?id={$_GET['id']}";
            $url = $base.$path;
            $session = curl_init($url);
            curl_setopt($session, CURLOPT_HEADER, false);
            curl_setopt($session, CURLOPT_RETURNTRANSFER, false);
            curl_exec($session);
        }
        
        curl_close($session);
    }
}
?>