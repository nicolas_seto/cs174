<?php
require_once('controller.php');
require_once($conf['DOC_ROOT'].'models/news_model.php');
require_once($conf['DOC_ROOT'].'views/main.php');

/**
 * The main controller class for SiteTest, which manages the site and
 * determines what the users will see.
 * Authors: Nicolas Seto, Dora Do
 */
class MainController implements Controller {
    private $model;
    private $view;
    private $news;
    
    function __construct() {
        $this->model = new NewsModel();
    }
    
    /**
     * Process the request based on request.
     */
    public function process() {
        $news = $this->model->getNews();
        $this->view = new MainView($news);
        $this->view->display();
    }
}
?>