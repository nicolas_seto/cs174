<?php
/* Authors: Nicolas Seto, Dora Do */
# Default values may be overridden per host
# Change the baseURL, ending with a forward slash, if necessary
# If the index.php is located in hw4 in the document root, enter
# as the following example: 'http://localhost/hw4/'
# Do not append index.php/ to the end.
$conf['baseURL'] = 'http://localhost/CS174/hw4/SiteTest/';
# Please specify the path to your Document Root.
# Also, if you do not deploy from the top-most level directory of the 
# Document Root, please make the necessary changes, ending with a forward 
# slash
# (e.g. "C:/your/path/Hw3/")
$conf['DOC_ROOT'] = '/opt/lampp/htdocs/CS174/hw4/SiteTest/';
# Please do the same for the BaseSiteAd url like for the baseURL. Do not
# append index.php/ to the end.
$conf['baseSiteURL'] = 'http://localhost/CS174/hw4/BestSiteAd/';
$conf['db_host'] = 'localhost';
$conf['db_user'] = 'root';
$conf['db_pass'] = 'password';
$conf['db_name'] = 'Ads';
# Decide whether you want to retrieve xml or json from BestSiteAd
$conf['format'] = 'xml';
?>