Authors: Nicolas Seto (007655531) & Dora Do (007684755)

Step 1: Place these files in the document root. Do not mess with the
folder organization.
Step 2: Go to the config folder and change the permissions to config.php
in case you can't edit it. chmod 777 config.php

Step 3: In config.php, change the baseURL, DOC_ROOT, baseSiteURL, 
db_host, db_user, format,
db_pass to fit your needs. If you place the files in the top level directory
of the document root (not inside the Hw4 folder), the baseURL would look like
"http://localhost/". If in another folder, like 
path/to/doc/root/Hw4/SiteTest, then it would look
like "http://localhost/Hw4/SiteTest/". Be sure to place the trailing 
forward slash.

The same goes for DOC_ROOT, if your document root path is 
"/opt/lampp/htdocs/", leave as is. Otherwise, add the subdirectories in which 
index.php is located in for this homework. If index.php and the folders are 
also in Hw4/SiteTest, then the path would be 
"/opt/lampp/htdocs/Hw4/SiteTest/". 
Be sure to add the forward trailing slash.

Step 4: In the terminal, in the config folder, run "php create.php". The
database should be created. Otherwise, you weren't authenticated as a user
in mysql. You should not change db_name.

Step 4: Type the baseURL into your browser and enjoy!

Side Note: To see the updated user rating, refresh the page. You can
submit your rating multiple times on the same page, and once you
refresh the page, the user rating will reflect the ratings you just
submitted.