<?php
require_once('model.php');
/**
 *  The base model for SiteTest. Has ability to retrieve news items.
 *  Authors: Nicolas Seto, Dora Do
 */
class NewsModel implements Model {
    
    function __construct() {
    
    }
    
    /**
     * Retrieves all the ads and its relevant information.
     */
    public function getNews() {
        global $conf;
        $con = mysqli_connect($conf['db_host'], $conf['db_user'], 
               $conf['db_pass'], $conf['db_name']);
        $query = "SELECT * FROM NewsStories ORDER BY RAND();";
        $result = mysqli_query($con, $query);
        while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
            $rows[] = $row;
        }
        mysqli_close($con);
        return $rows;
    }
}
?>