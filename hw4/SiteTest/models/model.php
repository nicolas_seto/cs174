<?php
/**
 *  The base model for SiteTest. Has ability to retrieve news stories.
 *  Authors: Nicolas Seto, Dora Do
 */
interface Model {
    /**
     * Retrieves all the news stories and its relevant information.
     */
    function getNews();
}
?>