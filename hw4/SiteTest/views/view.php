<?php
/** 
 * The base view for SiteTest.
 * Authors: Nicolas Seto, Dora Do
 */
interface View {
    /** 
     * Display the ten news stories and one ad.
     */
    function display();
}
?>