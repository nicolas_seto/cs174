<?php
require_once('view.php');

/**
 * The main view that displays news stories and an ad.
 */
class MainView implements View {
    private $news;
    
    function __construct($news) {
        $this->news = $news;
    }
    
    /** 
     * Display the ten news stories and one ad.
     */
    public function display() {
        global $conf;
        $list = $this->listNews();
        $html_code = <<<EOF
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" 
        href="{$conf['baseURL']}css/SiteTestStyle.css" />
        <script src="{$conf['baseURL']}views/scripts/SiteTestScript.js">
        </script>
        <title>SiteTest</title>
    </head>
    <body onload="loadAd()">
        <div id="header">SiteTest</div>
{$list}
   </body>
</html>
EOF;
        echo $html_code;
    }
    
    private function listNews() {
        global $conf;
        $string = "";
        
        for ($i = 0; $i < count($this->news); $i++) {
            $string .= <<<EOF
        <div class="article">
            Title: <a href="{$this->news[$i]['URL']}" 
            onclick="incrementStory(0);">{$this->news[$i]['Title']}</a>
            <br />
            Description: {$this->news[$i]['Description']}
        </div>

EOF;
            if ($i == 0) {
                $string .= <<<EOF
        <div id="advertisement">
        </div>

EOF;
            }
        }
        return $string;
        
    }
}

?>