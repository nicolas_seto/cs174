<?php
/* Authors: Nicolas Seto, Dora Do */
# Default values may be overridden per host
# Change the baseURL, ending with a forward slash, if necessary
# If the index.php is located in Hw3 in the document root, enter
# as the following example: 'http://localhost/Hw5/'
# Do not append index.php/ to the end of the url.
$conf['baseURL'] = 'http://localhost/CS174/hw5/';
# Please specify the path to your Document Root.
# Also, if you do not deploy from the top-most level directory of the 
# Document Root, please make the necessary changes, ending with a forward 
# slash
# (e.g. "C:/your/path/Hw5/")
$conf['doc_root'] = '/opt/lampp/htdocs/CS174/hw5/';
?>