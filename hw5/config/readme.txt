Authors: Nicolas Seto (007655531) & Dora Do (007684755)

Step 1: Place these files in the document root. Do not mess with the
folder organization.

Step 2: Go to the config folder and change the permissions to config.php
in case you can't edit it. chmod 777 config.php

Step 3: In config.php, change the baseURL and doc_root to fit your needs. 
If you place the files in the top level directory
of the document root (not inside the Hw5 folder), the baseURL would look like
"http://localhost/". If in another folder, like 
path/to/doc/root/Hw5/, then it would look
like "http://localhost/Hw5/". Be sure to place the trailing 
forward slash.

The same goes for doc_root, if your document root path is 
"/opt/lampp/htdocs/", leave as is. Otherwise, add the subdirectories in which 
index.php is located in for this homework. If index.php and the folders are 
also in Hw4/BaseSiteAd, then the path would be 
"/opt/lampp/htdocs/Hw5/". 
Be sure to add the forward trailing slash.

Step 4: Type the baseURL into your browser and enjoy!

Note: If you want to actually run create_sql_test.php, sql_test.php, and 
memcache_sql_test.php, you will have to change the permissions to 777, change 
the $db_host, $db_user, $db_pass as necessary for each file you want to run.
The shell scripts were used to automate the benchmarking process.