ab -n 100 -c 1 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient).....done


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        901 bytes

Concurrency Level:      1
Time taken for tests:   0.489 seconds
Complete requests:      100
Failed requests:        95
   (Connect: 0, Receive: 0, Length: 95, Exceptions: 0)
Write errors:           0
Total transferred:      96122 bytes
HTML transferred:       72922 bytes
Requests per second:    204.68 [#/sec] (mean)
Time per request:       4.886 [ms] (mean)
Time per request:       4.886 [ms] (mean, across all concurrent requests)
Transfer rate:          192.13 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       0
Processing:     2    5   4.3      3      31
Waiting:        0    3   2.7      3      14
Total:          2    5   4.3      3      31

Percentage of the requests served within a certain time (ms)
  50%      3
  66%      4
  75%      4
  80%      5
  90%     10
  95%     13
  98%     22
  99%     31
 100%     31 (longest request)
ab -n 100 -c 10 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient).....done


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        901 bytes

Concurrency Level:      10
Time taken for tests:   0.507 seconds
Complete requests:      100
Failed requests:        94
   (Connect: 0, Receive: 0, Length: 94, Exceptions: 0)
Write errors:           0
Total transferred:      96082 bytes
HTML transferred:       72882 bytes
Requests per second:    197.14 [#/sec] (mean)
Time per request:       50.726 [ms] (mean)
Time per request:       5.073 [ms] (mean, across all concurrent requests)
Transfer rate:          184.97 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    6  11.2      0      52
Processing:     4   43  28.5     38     183
Waiting:        0   40  28.4     36     183
Total:          8   49  27.6     44     186

Percentage of the requests served within a certain time (ms)
  50%     44
  66%     52
  75%     56
  80%     60
  90%     75
  95%    106
  98%    148
  99%    186
 100%    186 (longest request)
ab -n 100 -c 50 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient).....done


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        901 bytes

Concurrency Level:      50
Time taken for tests:   0.672 seconds
Complete requests:      100
Failed requests:        95
   (Connect: 0, Receive: 0, Length: 95, Exceptions: 0)
Write errors:           0
Total transferred:      96301 bytes
HTML transferred:       73101 bytes
Requests per second:    148.71 [#/sec] (mean)
Time per request:       336.233 [ms] (mean)
Time per request:       6.725 [ms] (mean, across all concurrent requests)
Transfer rate:          139.85 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0   25  25.9      4      54
Processing:    19  240 109.3    281     525
Waiting:       15  239 109.9    281     525
Total:         57  265  98.4    284     525

Percentage of the requests served within a certain time (ms)
  50%    284
  66%    301
  75%    311
  80%    317
  90%    344
  95%    436
  98%    523
  99%    525
 100%    525 (longest request)
ab -n 100 -c 100 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient).....done


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        905 bytes

Concurrency Level:      100
Time taken for tests:   0.471 seconds
Complete requests:      100
Failed requests:        95
   (Connect: 0, Receive: 0, Length: 95, Exceptions: 0)
Write errors:           0
Total transferred:      96098 bytes
HTML transferred:       72898 bytes
Requests per second:    212.37 [#/sec] (mean)
Time per request:       470.885 [ms] (mean)
Time per request:       4.709 [ms] (mean, across all concurrent requests)
Transfer rate:          199.30 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:       23   43   6.3     42      55
Processing:    24  233 116.0    238     408
Waiting:        7  231 117.9    238     407
Total:         62  276 112.7    279     449

Percentage of the requests served within a certain time (ms)
  50%    279
  66%    349
  75%    377
  80%    392
  90%    430
  95%    440
  98%    447
  99%    449
 100%    449 (longest request)
ab -n 500 -c 1 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        901 bytes

Concurrency Level:      1
Time taken for tests:   3.771 seconds
Complete requests:      500
Failed requests:        497
   (Connect: 0, Receive: 0, Length: 497, Exceptions: 0)
Write errors:           0
Total transferred:      472990 bytes
HTML transferred:       356990 bytes
Requests per second:    132.59 [#/sec] (mean)
Time per request:       7.542 [ms] (mean)
Time per request:       7.542 [ms] (mean, across all concurrent requests)
Transfer rate:          122.49 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       1
Processing:     2    7   5.1      5      42
Waiting:        0    6   5.0      4      34
Total:          2    7   5.1      6      42

Percentage of the requests served within a certain time (ms)
  50%      6
  66%      7
  75%      9
  80%     11
  90%     15
  95%     17
  98%     23
  99%     25
 100%     42 (longest request)
ab -n 500 -c 10 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        901 bytes

Concurrency Level:      10
Time taken for tests:   4.619 seconds
Complete requests:      500
Failed requests:        489
   (Connect: 0, Receive: 0, Length: 489, Exceptions: 0)
Write errors:           0
Total transferred:      474195 bytes
HTML transferred:       358195 bytes
Requests per second:    108.24 [#/sec] (mean)
Time per request:       92.384 [ms] (mean)
Time per request:       9.238 [ms] (mean, across all concurrent requests)
Transfer rate:          100.25 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    6  13.4      0      73
Processing:     4   86  64.2     67     417
Waiting:        0   81  63.0     65     417
Total:          4   92  64.6     73     417

Percentage of the requests served within a certain time (ms)
  50%     73
  66%     97
  75%    116
  80%    133
  90%    178
  95%    219
  98%    281
  99%    346
 100%    417 (longest request)
ab -n 500 -c 50 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        901 bytes

Concurrency Level:      50
Time taken for tests:   4.530 seconds
Complete requests:      500
Failed requests:        487
   (Connect: 0, Receive: 0, Length: 487, Exceptions: 0)
Write errors:           0
Total transferred:      477001 bytes
HTML transferred:       361001 bytes
Requests per second:    110.37 [#/sec] (mean)
Time per request:       453.040 [ms] (mean)
Time per request:       9.061 [ms] (mean, across all concurrent requests)
Transfer rate:          102.82 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0   31  43.2     17     239
Processing:    22  411 161.1    421    1018
Waiting:        8  382 149.0    399     923
Total:         43  442 148.1    451    1018

Percentage of the requests served within a certain time (ms)
  50%    451
  66%    502
  75%    543
  80%    568
  90%    622
  95%    665
  98%    746
  99%    826
 100%   1018 (longest request)
ab -n 500 -c 100 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        897 bytes

Concurrency Level:      100
Time taken for tests:   2.309 seconds
Complete requests:      500
Failed requests:        494
   (Connect: 0, Receive: 0, Length: 494, Exceptions: 0)
Write errors:           0
Total transferred:      475751 bytes
HTML transferred:       359751 bytes
Requests per second:    216.55 [#/sec] (mean)
Time per request:       461.785 [ms] (mean)
Time per request:       4.618 [ms] (mean, across all concurrent requests)
Transfer rate:          201.22 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0   51  52.2     37     171
Processing:    78  378 120.9    400     792
Waiting:       58  356 118.4    373     785
Total:        185  430  89.5    445     793

Percentage of the requests served within a certain time (ms)
  50%    445
  66%    471
  75%    481
  80%    486
  90%    522
  95%    550
  98%    608
  99%    689
 100%    793 (longest request)
ab -n 1000 -c 1 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        897 bytes

Concurrency Level:      1
Time taken for tests:   6.842 seconds
Complete requests:      1000
Failed requests:        990
   (Connect: 0, Receive: 0, Length: 990, Exceptions: 0)
Write errors:           0
Total transferred:      948319 bytes
HTML transferred:       716319 bytes
Requests per second:    146.15 [#/sec] (mean)
Time per request:       6.842 [ms] (mean)
Time per request:       6.842 [ms] (mean, across all concurrent requests)
Transfer rate:          135.35 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       2
Processing:     2    7   5.6      4      48
Waiting:        0    5   5.2      3      39
Total:          2    7   5.6      4      48

Percentage of the requests served within a certain time (ms)
  50%      4
  66%      6
  75%      9
  80%     10
  90%     14
  95%     17
  98%     25
  99%     28
 100%     48 (longest request)
ab -n 1000 -c 10 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        897 bytes

Concurrency Level:      10
Time taken for tests:   5.575 seconds
Complete requests:      1000
Failed requests:        991
   (Connect: 0, Receive: 0, Length: 991, Exceptions: 0)
Write errors:           0
Total transferred:      947224 bytes
HTML transferred:       715224 bytes
Requests per second:    179.38 [#/sec] (mean)
Time per request:       55.746 [ms] (mean)
Time per request:       5.575 [ms] (mean, across all concurrent requests)
Transfer rate:          165.93 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    7  11.6      0      63
Processing:     3   48  40.4     37     283
Waiting:        0   45  40.5     34     283
Total:          3   55  41.0     45     284

Percentage of the requests served within a certain time (ms)
  50%     45
  66%     57
  75%     66
  80%     72
  90%    102
  95%    146
  98%    195
  99%    215
 100%    284 (longest request)
ab -n 1000 -c 50 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        905 bytes

Concurrency Level:      50
Time taken for tests:   6.089 seconds
Complete requests:      1000
Failed requests:        983
   (Connect: 0, Receive: 0, Length: 983, Exceptions: 0)
Write errors:           0
Total transferred:      952222 bytes
HTML transferred:       720222 bytes
Requests per second:    164.23 [#/sec] (mean)
Time per request:       304.459 [ms] (mean)
Time per request:       6.089 [ms] (mean, across all concurrent requests)
Transfer rate:          152.71 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0   23  33.2      5     206
Processing:    17  277 143.4    258     957
Waiting:        2  260 143.5    229     949
Total:         63  300 130.0    269     957

Percentage of the requests served within a certain time (ms)
  50%    269
  66%    332
  75%    368
  80%    395
  90%    480
  95%    544
  98%    644
  99%    698
 100%    957 (longest request)
ab -n 1000 -c 100 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        901 bytes

Concurrency Level:      100
Time taken for tests:   7.400 seconds
Complete requests:      1000
Failed requests:        948
   (Connect: 0, Receive: 0, Length: 948, Exceptions: 0)
Write errors:           0
Total transferred:      962047 bytes
HTML transferred:       730047 bytes
Requests per second:    135.14 [#/sec] (mean)
Time per request:       739.991 [ms] (mean)
Time per request:       7.400 [ms] (mean, across all concurrent requests)
Transfer rate:          126.96 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0  134  96.9    136     431
Processing:    21  596 365.3    536    2034
Waiting:        2  524 352.8    452    2015
Total:        104  729 355.8    680    2137

Percentage of the requests served within a certain time (ms)
  50%    680
  66%    814
  75%    917
  80%   1042
  90%   1255
  95%   1393
  98%   1602
  99%   1716
 100%   2137 (longest request)
ab -n 2000 -c 1 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        905 bytes

Concurrency Level:      1
Time taken for tests:   11.845 seconds
Complete requests:      2000
Failed requests:        1952
   (Connect: 0, Receive: 0, Length: 1952, Exceptions: 0)
Write errors:           0
Total transferred:      1916263 bytes
HTML transferred:       1452263 bytes
Requests per second:    168.85 [#/sec] (mean)
Time per request:       5.922 [ms] (mean)
Time per request:       5.922 [ms] (mean, across all concurrent requests)
Transfer rate:          157.99 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.3      0       9
Processing:     2    6   4.4      4      36
Waiting:        0    4   4.2      3      29
Total:          2    6   4.4      4      37

Percentage of the requests served within a certain time (ms)
  50%      4
  66%      5
  75%      7
  80%      9
  90%     12
  95%     16
  98%     19
  99%     22
 100%     37 (longest request)
ab -n 2000 -c 10 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        905 bytes

Concurrency Level:      10
Time taken for tests:   12.955 seconds
Complete requests:      2000
Failed requests:        1962
   (Connect: 0, Receive: 0, Length: 1962, Exceptions: 0)
Write errors:           0
Total transferred:      1906773 bytes
HTML transferred:       1442773 bytes
Requests per second:    154.38 [#/sec] (mean)
Time per request:       64.777 [ms] (mean)
Time per request:       6.478 [ms] (mean, across all concurrent requests)
Transfer rate:          143.73 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    6  11.5      0     115
Processing:     3   58  57.0     42     443
Waiting:        0   54  56.4     38     443
Total:          3   64  57.7     47     443

Percentage of the requests served within a certain time (ms)
  50%     47
  66%     61
  75%     75
  80%     89
  90%    135
  95%    184
  98%    243
  99%    286
 100%    443 (longest request)
ab -n 2000 -c 50 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        905 bytes

Concurrency Level:      50
Time taken for tests:   10.690 seconds
Complete requests:      2000
Failed requests:        1964
   (Connect: 0, Receive: 0, Length: 1964, Exceptions: 0)
Write errors:           0
Total transferred:      1906223 bytes
HTML transferred:       1442223 bytes
Requests per second:    187.08 [#/sec] (mean)
Time per request:       267.258 [ms] (mean)
Time per request:       5.345 [ms] (mean, across all concurrent requests)
Transfer rate:          174.13 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0   45  50.1     30     250
Processing:    15  219 122.6    208     752
Waiting:        2  193 115.2    177     745
Total:         38  264 117.8    247     788

Percentage of the requests served within a certain time (ms)
  50%    247
  66%    298
  75%    331
  80%    350
  90%    431
  95%    483
  98%    553
  99%    604
 100%    788 (longest request)
ab -n 2000 -c 100 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        905 bytes

Concurrency Level:      100
Time taken for tests:   9.140 seconds
Complete requests:      2000
Failed requests:        1956
   (Connect: 0, Receive: 0, Length: 1956, Exceptions: 0)
Write errors:           0
Total transferred:      1901378 bytes
HTML transferred:       1437378 bytes
Requests per second:    218.82 [#/sec] (mean)
Time per request:       457.005 [ms] (mean)
Time per request:       4.570 [ms] (mean, across all concurrent requests)
Transfer rate:          203.15 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0   84  67.0     80     350
Processing:    13  367 159.0    371    1299
Waiting:        2  315 142.8    299    1159
Total:         60  451 134.4    439    1299

Percentage of the requests served within a certain time (ms)
  50%    439
  66%    489
  75%    515
  80%    531
  90%    610
  95%    691
  98%    763
  99%    833
 100%   1299 (longest request)
ab -n 5000 -c 1 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        897 bytes

Concurrency Level:      1
Time taken for tests:   22.968 seconds
Complete requests:      5000
Failed requests:        4900
   (Connect: 0, Receive: 0, Length: 4900, Exceptions: 0)
Write errors:           0
Total transferred:      4791767 bytes
HTML transferred:       3631767 bytes
Requests per second:    217.69 [#/sec] (mean)
Time per request:       4.594 [ms] (mean)
Time per request:       4.594 [ms] (mean, across all concurrent requests)
Transfer rate:          203.74 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.1      0       3
Processing:     2    4   3.1      3      32
Waiting:        0    3   3.0      3      29
Total:          2    5   3.1      3      33

Percentage of the requests served within a certain time (ms)
  50%      3
  66%      4
  75%      4
  80%      4
  90%     10
  95%     11
  98%     13
  99%     17
 100%     33 (longest request)
ab -n 5000 -c 10 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        901 bytes

Concurrency Level:      10
Time taken for tests:   23.330 seconds
Complete requests:      5000
Failed requests:        4878
   (Connect: 0, Receive: 0, Length: 4878, Exceptions: 0)
Write errors:           0
Total transferred:      4765119 bytes
HTML transferred:       3605119 bytes
Requests per second:    214.32 [#/sec] (mean)
Time per request:       46.660 [ms] (mean)
Time per request:       4.666 [ms] (mean, across all concurrent requests)
Transfer rate:          199.46 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    8  10.9      1      94
Processing:     2   39  37.3     29     422
Waiting:        0   35  36.9     26     415
Total:          2   46  37.3     39     449

Percentage of the requests served within a certain time (ms)
  50%     39
  66%     45
  75%     51
  80%     56
  90%     77
  95%    122
  98%    173
  99%    206
 100%    449 (longest request)
ab -n 5000 -c 50 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        901 bytes

Concurrency Level:      50
Time taken for tests:   21.631 seconds
Complete requests:      5000
Failed requests:        4898
   (Connect: 0, Receive: 0, Length: 4898, Exceptions: 0)
Write errors:           0
Total transferred:      4754885 bytes
HTML transferred:       3594885 bytes
Requests per second:    231.15 [#/sec] (mean)
Time per request:       216.314 [ms] (mean)
Time per request:       4.326 [ms] (mean, across all concurrent requests)
Transfer rate:          214.66 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0   48  45.2     40     225
Processing:    13  167  97.9    151     714
Waiting:        1  148  92.3    133     713
Total:         16  215  86.4    204     714

Percentage of the requests served within a certain time (ms)
  50%    204
  66%    237
  75%    256
  80%    272
  90%    328
  95%    376
  98%    444
  99%    490
 100%    714 (longest request)
ab -n 5000 -c 100 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        901 bytes

Concurrency Level:      100
Time taken for tests:   30.691 seconds
Complete requests:      5000
Failed requests:        4753
   (Connect: 0, Receive: 0, Length: 4753, Exceptions: 0)
Write errors:           0
Total transferred:      4692602 bytes
HTML transferred:       3532602 bytes
Requests per second:    162.92 [#/sec] (mean)
Time per request:       613.812 [ms] (mean)
Time per request:       6.138 [ms] (mean, across all concurrent requests)
Transfer rate:          149.32 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0  121 103.1    101     602
Processing:     2  486 308.0    426    2349
Waiting:        0  414 280.0    354    2241
Total:         10  607 296.3    569    2352

Percentage of the requests served within a certain time (ms)
  50%    569
  66%    693
  75%    784
  80%    837
  90%   1004
  95%   1123
  98%   1290
  99%   1428
 100%   2352 (longest request)
ab -n 10000 -c 1 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        901 bytes

Concurrency Level:      1
Time taken for tests:   58.380 seconds
Complete requests:      10000
Failed requests:        9676
   (Connect: 0, Receive: 0, Length: 9676, Exceptions: 0)
Write errors:           0
Total transferred:      9561015 bytes
HTML transferred:       7241015 bytes
Requests per second:    171.29 [#/sec] (mean)
Time per request:       5.838 [ms] (mean)
Time per request:       5.838 [ms] (mean, across all concurrent requests)
Transfer rate:          159.93 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    0   0.2      0      10
Processing:     2    6   4.6      4      67
Waiting:        0    4   4.5      3      57
Total:          2    6   4.6      4      67

Percentage of the requests served within a certain time (ms)
  50%      4
  66%      5
  75%      6
  80%      8
  90%     11
  95%     15
  98%     19
  99%     24
 100%     67 (longest request)
ab -n 10000 -c 10 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        905 bytes

Concurrency Level:      10
Time taken for tests:   46.802 seconds
Complete requests:      10000
Failed requests:        9949
   (Connect: 0, Receive: 0, Length: 9949, Exceptions: 0)
Write errors:           0
Total transferred:      9455244 bytes
HTML transferred:       7135244 bytes
Requests per second:    213.67 [#/sec] (mean)
Time per request:       46.802 [ms] (mean)
Time per request:       4.680 [ms] (mean, across all concurrent requests)
Transfer rate:          197.29 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    6   9.9      0      70
Processing:     2   40  38.9     30     444
Waiting:        0   37  38.6     27     443
Total:          2   47  38.7     37     444

Percentage of the requests served within a certain time (ms)
  50%     37
  66%     45
  75%     52
  80%     58
  90%     84
  95%    129
  98%    178
  99%    207
 100%    444 (longest request)
ab -n 10000 -c 50 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        897 bytes

Concurrency Level:      50
Time taken for tests:   44.754 seconds
Complete requests:      10000
Failed requests:        9890
   (Connect: 0, Receive: 0, Length: 9890, Exceptions: 0)
Write errors:           0
Total transferred:      9509138 bytes
HTML transferred:       7189138 bytes
Requests per second:    223.45 [#/sec] (mean)
Time per request:       223.768 [ms] (mean)
Time per request:       4.475 [ms] (mean, across all concurrent requests)
Transfer rate:          207.50 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0   45  46.2     32     267
Processing:     3  177 101.4    167     814
Waiting:        0  159  96.8    150     807
Total:          3  222  91.2    210     828

Percentage of the requests served within a certain time (ms)
  50%    210
  66%    243
  75%    270
  80%    287
  90%    337
  95%    390
  98%    450
  99%    513
 100%    828 (longest request)
ab -n 10000 -c 100 http://localhost/CS174/hw5/memcache_sql_test.php
This is ApacheBench, Version 2.3 <$Revision: 1430300 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        Apache/2.4.4
Server Hostname:        localhost
Server Port:            80

Document Path:          /CS174/hw5/memcache_sql_test.php
Document Length:        905 bytes

Concurrency Level:      100
Time taken for tests:   45.460 seconds
Complete requests:      10000
Failed requests:        9751
   (Connect: 0, Receive: 0, Length: 9751, Exceptions: 0)
Write errors:           0
Total transferred:      9443275 bytes
HTML transferred:       7123275 bytes
Requests per second:    219.98 [#/sec] (mean)
Time per request:       454.597 [ms] (mean)
Time per request:       4.546 [ms] (mean, across all concurrent requests)
Transfer rate:          202.86 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0  116  84.5    108     485
Processing:     1  336 179.2    319    1305
Waiting:        0  280 162.2    257    1152
Total:         11  452 169.3    446    1430

Percentage of the requests served within a certain time (ms)
  50%    446
  66%    506
  75%    556
  80%    589
  90%    664
  95%    736
  98%    827
  99%    880
 100%   1430 (longest request)
