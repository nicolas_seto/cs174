<?php
$db_host = 'localhost';
$db_user = 'root';
$db_pass = 'password';
$db_name = 'SQL_TEST';

$con = mysqli_connect($db_host, $db_user, $db_pass);

if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') ' . 
        mysqli_connect_error());
}

echo "Successfully connected to MySQL...\n";

$query_create_db = "DROP DATABASE IF EXISTS {$db_name};
    CREATE DATABASE IF NOT EXISTS {$db_name};";
    
if (mysqli_multi_query($con, $query_create_db)) {
    echo "Successfully created database {$db_name}...\n";
} else {
    echo "Error creating database: " . mysqli_error($con) . '\n';
}

/* Get rid of results of query */
while (mysqli_more_results($con) && mysqli_next_result($con));

/* Change to SQL_TEST database */
mysqli_select_db($con, $db_name);

/* Create tables */
$create_tables = <<<EOF
CREATE TABLE IF NOT EXISTS USER (ID CHAR(9) PRIMARY KEY, NAME VARCHAR(20));
CREATE TABLE IF NOT EXISTS USER_POST (USER_ID CHAR(9), TWEETS VARCHAR(140),
AT_WHAT_TIME INT, CONSTRAINT FOREIGN KEY(`USER_ID`) REFERENCES `USER`(`ID`));
EOF;

if (mysqli_multi_query($con, $create_tables)) {
    echo "Successfully created tables...\n";
} else {
    echo "Error creating tables:" . mysqli_error($con) ."\n";
}

/* Get rid of results of query */
while (mysqli_more_results($con) && mysqli_next_result($con));

echo "Populating USER table...\n";

$query = <<<EOF
INSERT INTO `USER`(`ID`,`NAME`) VALUES(1, 'bob');
INSERT INTO `USER`(`ID`,`NAME`) VALUES(2, 'sally');
INSERT INTO `USER`(`ID`,`NAME`) VALUES(3, 'frank');
EOF;

if (mysqli_multi_query($con, $query)) {
    echo "Successfully added users to USER table...\n";
} else {
    echo "Error populating USER:" . mysqli_error($con) ."\n";
}

/* Get rid of results of query */
while (mysqli_more_results($con) && mysqli_next_result($con));

for ($i = 1; $i < 4; $i++) {
    for ($j = 1; $j < 31; $j++) {
        $time = time();
        $query = "INSERT INTO USER_POST(USER_ID, TWEETS, AT_WHAT_TIME)
            VALUES({$i}, 'Tweet #{$j}', {$time});";
        if (mysqli_query($con, $query)) {
            echo "Added Tweet #{$j} to {$i}\n";
        } else {
            echo "Error inserting values to {$i}:".mysqli_error($con) ."\n";
        }
        sleep(1);
    }
}

mysqli_close($con);
?>