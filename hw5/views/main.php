<?php
require_once('view.php');

/**
 * The main view that displays the click totals and submission form.
 */
class MainView implements View {

    function __construct() {

    }
    
    /**
     * Display raw data and analyses.
     */
    public function display() {
        global $conf;
        $html_code = <<<EOF
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width; 
        height=device-height; maximum-scale=1.4; 
        initial-scale=1.0; user-scalable=yes" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black" />
        <script type="text/javascript" 
        src="{$conf['baseURL']}views/scripts/script.js"></script>
        <title>Mobile Experiment</title>
        <link rel="stylesheet" href=
        "http://code.jquery.com/mobile/1.3.1/jquery.mobile-1.3.1.min.css">
        <link rel="stylesheet" href="{$conf['baseURL']}css/style.css">
        <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script 
        src="http://code.jquery.com/mobile/1.3.1/jquery.mobile-1.3.1.min.js">
        </script>
    </head>
    <body>
        <!-- Home -->
        <div data-role="page" id="page1">
            <div data-theme="a" data-role="header">
                <h3>
                    Experiments for HW5 -- CS174
                </h3>
            </div>
            <div data-role="content">
                <canvas id="myCanvas">Your browser does not support 
                the HTML5 canvas tag.</canvas>
                <script>
                setCanvas();
                </script>
                <div data-role="collapsible-set" data-theme="a" 
                data-content-theme="e">
                    <div data-role="collapsible" data-collapsed="true">
                        <h3>
                            test.html Benchmark Results (RAW DATA)
                        </h3>
ab -n 100 -c 1 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient).....done<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   0.200 seconds<br />
Complete requests:      100<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      2078000 bytes<br />
HTML transferred:       2047500 bytes<br />
Requests per second:    500.95 [#/sec] (mean)<br />
Time per request:       1.996 [ms] (mean)<br />
Time per request:      1.996 [ms] (mean across all concurrent requests)<br />
Transfer rate:          10165.80 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.2      0       1<br />
Processing:     0    2   3.4      1      22<br />
Waiting:        0    0   2.2      0      22<br />
Total:          0    2   3.4      1      22<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      1<br />
  66%      1<br />
  75%      2<br />
  80%      2<br />
  90%      4<br />
  95%     10<br />
  98%     16<br />
  99%     22<br />
 100%     22 (longest request)<br />
ab -n 100 -c 10 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient).....done<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   0.188 seconds<br />
Complete requests:      100<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      2078000 bytes<br />
HTML transferred:       2047500 bytes<br />
Requests per second:    531.13 [#/sec] (mean)<br />
Time per request:       18.828 [ms] (mean)<br />
Time per request:      1.883 [ms] (mean across all concurrent requests)<br />
Transfer rate:          10778.25 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    6   8.1      3      37<br />
Processing:     2   12   9.1     10      44<br />
Waiting:        0    9   7.6      8      43<br />
Total:          3   18  13.7     13      81<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     13<br />
  66%     16<br />
  75%     19<br />
  80%     31<br />
  90%     40<br />
  95%     43<br />
  98%     70<br />
  99%     81<br />
 100%     81 (longest request)<br />
ab -n 100 -c 50 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient).....done<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   0.197 seconds<br />
Complete requests:      100<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      2078000 bytes<br />
HTML transferred:       2047500 bytes<br />
Requests per second:    506.68 [#/sec] (mean)<br />
Time per request:       98.683 [ms] (mean)<br />
Time per request:      1.974 [ms] (mean across all concurrent requests)<br />
Transfer rate:          10281.95 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   10   8.3     12      40<br />
Processing:    13   68  29.3     70     139<br />
Waiting:        2   59  29.3     61     124<br />
Total:         23   78  29.3     76     152<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     76<br />
  66%    101<br />
  75%    103<br />
  80%    110<br />
  90%    114<br />
  95%    115<br />
  98%    120<br />
  99%    152<br />
 100%    152 (longest request)<br />
ab -n 100 -c 100 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient).....done<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   0.139 seconds<br />
Complete requests:      100<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      2078000 bytes<br />
HTML transferred:       2047500 bytes<br />
Requests per second:    721.86 [#/sec] (mean)<br />
Time per request:       138.531 [ms] (mean)<br />
Time per request:      1.385 [ms] (mean across all concurrent requests)<br />
Transfer rate:          14648.68 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        9   23   2.6     24      25<br />
Processing:    43   78  18.2     78     106<br />
Waiting:       23   75  21.1     78     106<br />
Total:         52  101  19.1    101     130<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    101<br />
  66%    108<br />
  75%    120<br />
  80%    124<br />
  90%    129<br />
  95%    130<br />
  98%    130<br />
  99%    130<br />
 100%    130 (longest request)<br />
<br />
ab -n 500 -c 1 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 100 requests<br />
Completed 200 requests<br />
Completed 300 requests<br />
Completed 400 requests<br />
Completed 500 requests<br />
Finished 500 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   1.262 seconds<br />
Complete requests:      500<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      10390000 bytes<br />
HTML transferred:       10237500 bytes<br />
Requests per second:    396.27 [#/sec] (mean)<br />
Time per request:       2.524 [ms] (mean)<br />
Time per request:      2.524 [ms] (mean across all concurrent requests)<br />
Transfer rate:          8041.42 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   1.0      0      21<br />
Processing:     0    2   4.5      1      50<br />
Waiting:        0    0   1.4      0      18<br />
Total:          0    2   4.5      1      50<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      1<br />
  66%      1<br />
  75%      2<br />
  80%      2<br />
  90%      3<br />
  95%      8<br />
  98%     19<br />
  99%     22<br />
 100%     50 (longest request)<br />
ab -n 500 -c 10 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 100 requests<br />
Completed 200 requests<br />
Completed 300 requests<br />
Completed 400 requests<br />
Completed 500 requests<br />
Finished 500 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   0.741 seconds<br />
Complete requests:      500<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      10390000 bytes<br />
HTML transferred:       10237500 bytes<br />
Requests per second:    675.13 [#/sec] (mean)<br />
Time per request:       14.812 [ms] (mean)<br />
Time per request:      1.481 [ms] (mean across all concurrent requests)<br />
Transfer rate:          13700.38 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    5   6.3      3      28<br />
Processing:     1    9   7.7      6      62<br />
Waiting:        0    6   7.2      3      62<br />
Total:          3   14   9.0     12      63<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     12<br />
  66%     16<br />
  75%     19<br />
  80%     21<br />
  90%     29<br />
  95%     31<br />
  98%     37<br />
  99%     48<br />
 100%     63 (longest request)<br />
ab -n 500 -c 50 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 100 requests<br />
Completed 200 requests<br />
Completed 300 requests<br />
Completed 400 requests<br />
Completed 500 requests<br />
Finished 500 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   0.686 seconds<br />
Complete requests:      500<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      10390000 bytes<br />
HTML transferred:       10237500 bytes<br />
Requests per second:    729.02 [#/sec] (mean)<br />
Time per request:       68.585 [ms] (mean)<br />
Time per request:      1.372 [ms] (mean across all concurrent requests)<br />
Transfer rate:          14794.05 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    7   6.1      5      35<br />
Processing:    12   58  15.8     60      88<br />
Waiting:        2   52  15.6     52      88<br />
Total:         17   65  15.1     66      95<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     66<br />
  66%     72<br />
  75%     77<br />
  80%     80<br />
  90%     84<br />
  95%     88<br />
  98%     89<br />
  99%     90<br />
 100%     95 (longest request)<br />
ab -n 500 -c 100 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 100 requests<br />
Completed 200 requests<br />
Completed 300 requests<br />
Completed 400 requests<br />
Completed 500 requests<br />
Finished 500 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   0.721 seconds<br />
Complete requests:      500<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      10390000 bytes<br />
HTML transferred:       10237500 bytes<br />
Requests per second:    693.62 [#/sec] (mean)<br />
Time per request:       144.171 [ms] (mean)<br />
Time per request:      1.442 [ms] (mean across all concurrent requests)<br />
Transfer rate:          14075.62 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    7   5.5      6      25<br />
Processing:    12  123  31.1    119     196<br />
Waiting:        7  117  31.7    114     195<br />
Total:         22  131  30.9    125     201<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    125<br />
  66%    134<br />
  75%    142<br />
  80%    150<br />
  90%    179<br />
  95%    187<br />
  98%    197<br />
  99%    199<br />
 100%    201 (longest request)<br />
<br />
ab -n 1000 -c 1 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 100 requests<br />
Completed 200 requests<br />
Completed 300 requests<br />
Completed 400 requests<br />
Completed 500 requests<br />
Completed 600 requests<br />
Completed 700 requests<br />
Completed 800 requests<br />
Completed 900 requests<br />
Completed 1000 requests<br />
Finished 1000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   1.742 seconds<br />
Complete requests:      1000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      20780000 bytes<br />
HTML transferred:       20475000 bytes<br />
Requests per second:    574.12 [#/sec] (mean)<br />
Time per request:       1.742 [ms] (mean)<br />
Time per request:      1.742 [ms] (mean across all concurrent requests)<br />
Transfer rate:          11650.69 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.3      0       7<br />
Processing:     0    1   2.8      1      26<br />
Waiting:        0    0   0.5      0       9<br />
Total:          0    1   2.8      1      26<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      1<br />
  66%      1<br />
  75%      1<br />
  80%      2<br />
  90%      2<br />
  95%      5<br />
  98%     13<br />
  99%     15<br />
 100%     26 (longest request)<br />
ab -n 1000 -c 10 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 100 requests<br />
Completed 200 requests<br />
Completed 300 requests<br />
Completed 400 requests<br />
Completed 500 requests<br />
Completed 600 requests<br />
Completed 700 requests<br />
Completed 800 requests<br />
Completed 900 requests<br />
Completed 1000 requests<br />
Finished 1000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   2.033 seconds<br />
Complete requests:      1000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      20780000 bytes<br />
HTML transferred:       20475000 bytes<br />
Requests per second:    491.95 [#/sec] (mean)<br />
Time per request:       20.327 [ms] (mean)<br />
Time per request:      2.033 [ms] (mean across all concurrent requests)<br />
Transfer rate:          9983.07 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    6   6.9      3      43<br />
Processing:     1   14  11.8     11      84<br />
Waiting:        0    9  10.5      5      73<br />
Total:          1   20  13.4     17      96<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     17<br />
  66%     21<br />
  75%     25<br />
  80%     28<br />
  90%     37<br />
  95%     44<br />
  98%     58<br />
  99%     71<br />
 100%     96 (longest request)<br />
ab -n 1000 -c 50 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 100 requests<br />
Completed 200 requests<br />
Completed 300 requests<br />
Completed 400 requests<br />
Completed 500 requests<br />
Completed 600 requests<br />
Completed 700 requests<br />
Completed 800 requests<br />
Completed 900 requests<br />
Completed 1000 requests<br />
Finished 1000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   1.638 seconds<br />
Complete requests:      1000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      20780000 bytes<br />
HTML transferred:       20475000 bytes<br />
Requests per second:    610.34 [#/sec] (mean)<br />
Time per request:       81.922 [ms] (mean)<br />
Time per request:      1.638 [ms] (mean across all concurrent requests)<br />
Transfer rate:          12385.57 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    8   8.7      5      56<br />
Processing:    18   71  28.2     64     188<br />
Waiting:       13   63  26.1     56     181<br />
Total:         40   79  28.9     73     190<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     73<br />
  66%     84<br />
  75%     89<br />
  80%     95<br />
  90%    115<br />
  95%    136<br />
  98%    177<br />
  99%    186<br />
 100%    190 (longest request)<br />
ab -n 1000 -c 100 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 100 requests<br />
Completed 200 requests<br />
Completed 300 requests<br />
Completed 400 requests<br />
Completed 500 requests<br />
Completed 600 requests<br />
Completed 700 requests<br />
Completed 800 requests<br />
Completed 900 requests<br />
Completed 1000 requests<br />
Finished 1000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   1.770 seconds<br />
Complete requests:      1000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      20780000 bytes<br />
HTML transferred:       20475000 bytes<br />
Requests per second:    565.09 [#/sec] (mean)<br />
Time per request:       176.963 [ms] (mean)<br />
Time per request:      1.770 [ms] (mean across all concurrent requests)<br />
Transfer rate:          11467.36 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   11  11.2      6      57<br />
Processing:    16  156  41.9    158     247<br />
Waiting:       14  148  40.8    150     233<br />
Total:         45  167  37.6    167     258<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    167<br />
  66%    180<br />
  75%    188<br />
  80%    194<br />
  90%    218<br />
  95%    228<br />
  98%    235<br />
  99%    240<br />
 100%    258 (longest request)<br />
<br />
ab -n 2000 -c 1 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 200 requests<br />
Completed 400 requests<br />
Completed 600 requests<br />
Completed 800 requests<br />
Completed 1000 requests<br />
Completed 1200 requests<br />
Completed 1400 requests<br />
Completed 1600 requests<br />
Completed 1800 requests<br />
Completed 2000 requests<br />
Finished 2000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   3.582 seconds<br />
Complete requests:      2000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      41560000 bytes<br />
HTML transferred:       40950000 bytes<br />
Requests per second:    558.39 [#/sec] (mean)<br />
Time per request:       1.791 [ms] (mean)<br />
Time per request:      1.791 [ms] (mean across all concurrent requests)<br />
Transfer rate:          11331.37 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.5      0      22<br />
Processing:     0    1   3.3      1      56<br />
Waiting:        0    0   1.5      0      39<br />
Total:          0    1   3.3      1      56<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      1<br />
  66%      1<br />
  75%      1<br />
  80%      1<br />
  90%      2<br />
  95%      4<br />
  98%     12<br />
  99%     15<br />
 100%     56 (longest request)<br />
ab -n 2000 -c 10 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 200 requests<br />
Completed 400 requests<br />
Completed 600 requests<br />
Completed 800 requests<br />
Completed 1000 requests<br />
Completed 1200 requests<br />
Completed 1400 requests<br />
Completed 1600 requests<br />
Completed 1800 requests<br />
Completed 2000 requests<br />
Finished 2000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   3.633 seconds<br />
Complete requests:      2000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      41560000 bytes<br />
HTML transferred:       40950000 bytes<br />
Requests per second:    550.58 [#/sec] (mean)<br />
Time per request:       18.163 [ms] (mean)<br />
Time per request:      1.816 [ms] (mean across all concurrent requests)<br />
Transfer rate:          11172.96 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    6   6.6      4      41<br />
Processing:     1   12   9.4      9      72<br />
Waiting:        0    7   8.3      4      71<br />
Total:          1   18  11.3     15      82<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     15<br />
  66%     19<br />
  75%     23<br />
  80%     25<br />
  90%     34<br />
  95%     42<br />
  98%     47<br />
  99%     54<br />
 100%     82 (longest request)<br />
ab -n 2000 -c 50 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 200 requests<br />
Completed 400 requests<br />
Completed 600 requests<br />
Completed 800 requests<br />
Completed 1000 requests<br />
Completed 1200 requests<br />
Completed 1400 requests<br />
Completed 1600 requests<br />
Completed 1800 requests<br />
Completed 2000 requests<br />
Finished 2000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   1.460 seconds<br />
Complete requests:      2000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      41560000 bytes<br />
HTML transferred:       40950000 bytes<br />
Requests per second:    1369.71 [#/sec] (mean)<br />
Time per request:       36.504 [ms] (mean)<br />
Time per request:      0.730 [ms] (mean across all concurrent requests)<br />
Transfer rate:          27795.41 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    4   4.8      2      32<br />
Processing:    11   32  12.5     28      71<br />
Waiting:        4   28  11.8     25      70<br />
Total:         14   35  12.9     32      72<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     32<br />
  66%     38<br />
  75%     45<br />
  80%     48<br />
  90%     53<br />
  95%     62<br />
  98%     68<br />
  99%     70<br />
 100%     72 (longest request)<br />
ab -n 2000 -c 100 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 200 requests<br />
Completed 400 requests<br />
Completed 600 requests<br />
Completed 800 requests<br />
Completed 1000 requests<br />
Completed 1200 requests<br />
Completed 1400 requests<br />
Completed 1600 requests<br />
Completed 1800 requests<br />
Completed 2000 requests<br />
Finished 2000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   1.598 seconds<br />
Complete requests:      2000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      41560000 bytes<br />
HTML transferred:       40950000 bytes<br />
Requests per second:    1251.57 [#/sec] (mean)<br />
Time per request:       79.900 [ms] (mean)<br />
Time per request:      0.799 [ms] (mean across all concurrent requests)<br />
Transfer rate:          25397.97 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    5   7.1      3      42<br />
Processing:    10   73  21.3     70     144<br />
Waiting:        6   69  20.8     66     137<br />
Total:         37   78  19.5     74     144<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     74<br />
  66%     82<br />
  75%     88<br />
  80%     94<br />
  90%    110<br />
  95%    116<br />
  98%    124<br />
  99%    128<br />
 100%    144 (longest request)<br />
 <br />
ab -n 5000 -c 1 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 500 requests<br />
Completed 1000 requests<br />
Completed 1500 requests<br />
Completed 2000 requests<br />
Completed 2500 requests<br />
Completed 3000 requests<br />
Completed 3500 requests<br />
Completed 4000 requests<br />
Completed 4500 requests<br />
Completed 5000 requests<br />
Finished 5000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   4.092 seconds<br />
Complete requests:      5000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      103900000 bytes<br />
HTML transferred:       102375000 bytes<br />
Requests per second:    1221.93 [#/sec] (mean)<br />
Time per request:       0.818 [ms] (mean)<br />
Time per request:       0.818 [ms] (mean across all concurrent requests)<br />
Transfer rate:          24796.58 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.2      0       6<br />
Processing:     0    1   1.5      0      27<br />
Waiting:        0    0   0.6      0      14<br />
Total:          0    1   1.5      0      27<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      0<br />
  66%      0<br />
  75%      1<br />
  80%      1<br />
  90%      1<br />
  95%      2<br />
  98%      8<br />
  99%      9<br />
 100%     27 (longest request)<br />
ab -n 5000 -c 10 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 500 requests<br />
Completed 1000 requests<br />
Completed 1500 requests<br />
Completed 2000 requests<br />
Completed 2500 requests<br />
Completed 3000 requests<br />
Completed 3500 requests<br />
Completed 4000 requests<br />
Completed 4500 requests<br />
Completed 5000 requests<br />
Finished 5000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   4.437 seconds<br />
Complete requests:      5000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      103900000 bytes<br />
HTML transferred:       102375000 bytes<br />
Requests per second:    1126.80 [#/sec] (mean)<br />
Time per request:       8.875 [ms] (mean)<br />
Time per request:       0.887 [ms] (mean across all concurrent requests)<br />
Transfer rate:          22866.08 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    3   3.9      2      35<br />
Processing:     0    5   4.9      4      63<br />
Waiting:        0    3   4.3      2      48<br />
Total:          1    9   6.6      6      78<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      6<br />
  66%      8<br />
  75%     11<br />
  80%     13<br />
  90%     16<br />
  95%     21<br />
  98%     29<br />
  99%     36<br />
 100%     78 (longest request)<br />
ab -n 5000 -c 50 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 500 requests<br />
Completed 1000 requests<br />
Completed 1500 requests<br />
Completed 2000 requests<br />
Completed 2500 requests<br />
Completed 3000 requests<br />
Completed 3500 requests<br />
Completed 4000 requests<br />
Completed 4500 requests<br />
Completed 5000 requests<br />
Finished 5000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   3.520 seconds<br />
Complete requests:      5000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      103900000 bytes<br />
HTML transferred:       102375000 bytes<br />
Requests per second:    1420.28 [#/sec] (mean)<br />
Time per request:       35.204 [ms] (mean)<br />
Time per request:       0.704 [ms] (mean across all concurrent requests)<br />
Transfer rate:          28821.79 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    5   4.2      3      35<br />
Processing:    11   30  10.1     28      86<br />
Waiting:        1   25   9.5     23      77<br />
Total:         16   35  10.5     32      91<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     32<br />
  66%     36<br />
  75%     40<br />
  80%     41<br />
  90%     48<br />
  95%     54<br />
  98%     62<br />
  99%     73<br />
 100%     91 (longest request)<br />
ab -n 5000 -c 100 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 500 requests<br />
Completed 1000 requests<br />
Completed 1500 requests<br />
Completed 2000 requests<br />
Completed 2500 requests<br />
Completed 3000 requests<br />
Completed 3500 requests<br />
Completed 4000 requests<br />
Completed 4500 requests<br />
Completed 5000 requests<br />
Finished 5000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   3.478 seconds<br />
Complete requests:      5000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      103900000 bytes<br />
HTML transferred:       102375000 bytes<br />
Requests per second:    1437.49 [#/sec] (mean)<br />
Time per request:       69.566 [ms] (mean)<br />
Time per request:       0.696 [ms] (mean across all concurrent requests)<br />
Transfer rate:          29170.91 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    7   5.6      6      41<br />
Processing:    10   61  16.1     60     125<br />
Waiting:        2   53  15.4     52     124<br />
Total:         24   69  16.0     67     131<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     67<br />
  66%     71<br />
  75%     76<br />
  80%     79<br />
  90%     90<br />
  95%     99<br />
  98%    110<br />
  99%    122<br />
 100%    131 (longest request)<br />
<br />
ab -n 10000 -c 1 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 1000 requests<br />
Completed 2000 requests<br />
Completed 3000 requests<br />
Completed 4000 requests<br />
Completed 5000 requests<br />
Completed 6000 requests<br />
Completed 7000 requests<br />
Completed 8000 requests<br />
Completed 9000 requests<br />
Completed 10000 requests<br />
Finished 10000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   8.662 seconds<br />
Complete requests:      10000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      207800000 bytes<br />
HTML transferred:       204750000 bytes<br />
Requests per second:    1154.49 [#/sec] (mean)<br />
Time per request:       0.866 [ms] (mean)<br />
Time per request:       0.866 [ms] (mean across all concurrent requests)<br />
Transfer rate:          23427.94 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.3      0      25<br />
Processing:     0    1   1.7      0      42<br />
Waiting:        0    0   0.7      0      22<br />
Total:          0    1   1.8      0      42<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      0<br />
  66%      0<br />
  75%      1<br />
  80%      1<br />
  90%      1<br />
  95%      2<br />
  98%      8<br />
  99%     10<br />
 100%     42 (longest request)<br />
ab -n 10000 -c 10 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 1000 requests<br />
Completed 2000 requests<br />
Completed 3000 requests<br />
Completed 4000 requests<br />
Completed 5000 requests<br />
Completed 6000 requests<br />
Completed 7000 requests<br />
Completed 8000 requests<br />
Completed 9000 requests<br />
Completed 10000 requests<br />
Finished 10000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   8.981 seconds<br />
Complete requests:      10000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      207800000 bytes<br />
HTML transferred:       204750000 bytes<br />
Requests per second:    1113.47 [#/sec] (mean)<br />
Time per request:       8.981 [ms] (mean)<br />
Time per request:       0.898 [ms] (mean across all concurrent requests)<br />
Transfer rate:          22595.59 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    3   3.8      2      39<br />
Processing:     0    6   5.4      3      62<br />
Waiting:        0    3   4.7      2      58<br />
Total:          1    9   6.7      6      71<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      6<br />
  66%      8<br />
  75%     12<br />
  80%     13<br />
  90%     17<br />
  95%     22<br />
  98%     30<br />
  99%     36<br />
 100%     71 (longest request)<br />
ab -n 10000 -c 50 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 1000 requests<br />
Completed 2000 requests<br />
Completed 3000 requests<br />
Completed 4000 requests<br />
Completed 5000 requests<br />
Completed 6000 requests<br />
Completed 7000 requests<br />
Completed 8000 requests<br />
Completed 9000 requests<br />
Completed 10000 requests<br />
Finished 10000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   7.969 seconds<br />
Complete requests:      10000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      207800000 bytes<br />
HTML transferred:       204750000 bytes<br />
Requests per second:    1254.84 [#/sec] (mean)<br />
Time per request:       39.846 [ms] (mean)<br />
Time per request:       0.797 [ms] (mean across all concurrent requests)<br />
Transfer rate:          25464.50 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    8   7.8      5      63<br />
Processing:     5   31  17.1     27     182<br />
Waiting:        0   23  16.5     19     179<br />
Total:         17   39  17.8     36     184<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     36<br />
  66%     41<br />
  75%     45<br />
  80%     49<br />
  90%     58<br />
  95%     70<br />
  98%     94<br />
  99%    101<br />
 100%    184 (longest request)<br />
ab -n 10000 -c 100 http://localhost/CS174/hw5/test.html<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 1000 requests<br />
Completed 2000 requests<br />
Completed 3000 requests<br />
Completed 4000 requests<br />
Completed 5000 requests<br />
Completed 6000 requests<br />
Completed 7000 requests<br />
Completed 8000 requests<br />
Completed 9000 requests<br />
Completed 10000 requests<br />
Finished 10000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.html<br />
Document Length:        20475 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   7.279 seconds<br />
Complete requests:      10000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      207800000 bytes<br />
HTML transferred:       204750000 bytes<br />
Requests per second:    1373.72 [#/sec] (mean)<br />
Time per request:       72.795 [ms] (mean)<br />
Time per request:       0.728 [ms] (mean across all concurrent requests)<br />
Transfer rate:          27876.91 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   17  12.5     15     131<br />
Processing:    12   55  23.9     50     188<br />
Waiting:        0   36  22.6     33     179<br />
Total:         32   72  24.7     65     202<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     65<br />
  66%     75<br />
  75%     82<br />
  80%     88<br />
  90%    104<br />
  95%    117<br />
  98%    140<br />
  99%    174<br />
 100%    202 (longest request)<br />
<br />
<br />
                    </div>
                    <div data-role="collapsible" data-collapsed="true">
                        <h3>
                            test.php Benchmark Results (RAW DATA)
                        </h3>
ab -n 100 -c 1 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient).....done<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   0.133 seconds<br />
Complete requests:      100<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      2070600 bytes<br />
HTML transferred:       2049500 bytes<br />
Requests per second:    753.66 [#/sec] (mean)<br />
Time per request:       1.327 [ms] (mean)<br />
Time per request:       1.327 [ms] (mean across all concurrent requests)<br />
Transfer rate:          15239.63 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.2      0       1<br />
Processing:     0    1   2.1      1      16<br />
Waiting:        0    0   0.4      0       4<br />
Total:          0    1   2.1      1      16<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      1<br />
  66%      1<br />
  75%      1<br />
  80%      2<br />
  90%      2<br />
  95%      2<br />
  98%     12<br />
  99%     16<br />
 100%     16 (longest request)<br />
ab -n 100 -c 10 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient).....done<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   0.162 seconds<br />
Complete requests:      100<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      2070600 bytes<br />
HTML transferred:       2049500 bytes<br />
Requests per second:    616.79 [#/sec] (mean)<br />
Time per request:       16.213 [ms] (mean)<br />
Time per request:       1.621 [ms] (mean across all concurrent requests)<br />
Transfer rate:          12471.83 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    4   5.6      2      28<br />
Processing:     2   11   8.2      9      61<br />
Waiting:        0    8   6.5      7      42<br />
Total:          5   16   9.0     14      61<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     14<br />
  66%     17<br />
  75%     19<br />
  80%     21<br />
  90%     23<br />
  95%     33<br />
  98%     53<br />
  99%     61<br />
 100%     61 (longest request)<br />
ab -n 100 -c 50 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient).....done<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   0.137 seconds<br />
Complete requests:      100<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      2070600 bytes<br />
HTML transferred:       2049500 bytes<br />
Requests per second:    732.12 [#/sec] (mean)<br />
Time per request:       68.294 [ms] (mean)<br />
Time per request:       1.366 [ms] (mean across all concurrent requests)<br />
Transfer rate:          14804.05 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   16  16.7     12      43<br />
Processing:     7   35  13.9     40      59<br />
Waiting:        1   31  13.8     36      53<br />
Total:         41   51   7.0     51      70<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     51<br />
  66%     51<br />
  75%     53<br />
  80%     58<br />
  90%     62<br />
  95%     68<br />
  98%     70<br />
  99%     70<br />
 100%     70 (longest request)<br />
ab -n 100 -c 100 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient).....done<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   0.174 seconds<br />
Complete requests:      100<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      2070600 bytes<br />
HTML transferred:       2049500 bytes<br />
Requests per second:    573.09 [#/sec] (mean)<br />
Time per request:       174.492 [ms] (mean)<br />
Time per request:       1.745 [ms] (mean across all concurrent requests)<br />
Transfer rate:          11588.33 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        5   25   4.2     25      32<br />
Processing:    20   87  45.3    103     140<br />
Waiting:       19   82  43.8    100     137<br />
Total:         37  112  46.7    127     169<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    127<br />
  66%    147<br />
  75%    153<br />
  80%    158<br />
  90%    168<br />
  95%    169<br />
  98%    169<br />
  99%    169<br />
 100%    169 (longest request)<br />
 <br />
ab -n 500 -c 1 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 100 requests<br />
Completed 200 requests<br />
Completed 300 requests<br />
Completed 400 requests<br />
Completed 500 requests<br />
Finished 500 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   0.818 seconds<br />
Complete requests:      500<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      10353000 bytes<br />
HTML transferred:       10247500 bytes<br />
Requests per second:    611.25 [#/sec] (mean)<br />
Time per request:       1.636 [ms] (mean)<br />
Time per request:       1.636 [ms] (mean across all concurrent requests)<br />
Transfer rate:          12359.83 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.2      0       3<br />
Processing:     0    1   2.6      1      32<br />
Waiting:        0    0   0.2      0       1<br />
Total:          0    2   2.6      1      32<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      1<br />
  66%      1<br />
  75%      1<br />
  80%      1<br />
  90%      2<br />
  95%      5<br />
  98%     10<br />
  99%     13<br />
 100%     32 (longest request)<br />
ab -n 500 -c 10 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 100 requests<br />
Completed 200 requests<br />
Completed 300 requests<br />
Completed 400 requests<br />
Completed 500 requests<br />
Finished 500 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   0.786 seconds<br />
Complete requests:      500<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      10353000 bytes<br />
HTML transferred:       10247500 bytes<br />
Requests per second:    635.99 [#/sec] (mean)<br />
Time per request:       15.723 [ms] (mean)<br />
Time per request:       1.572 [ms] (mean across all concurrent requests)<br />
Transfer rate:          12860.24 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    4   4.5      2      24<br />
Processing:     2   12   7.4     10      50<br />
Waiting:        0    6   6.2      4      39<br />
Total:          3   15   8.8     13      59<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     13<br />
  66%     18<br />
  75%     21<br />
  80%     23<br />
  90%     28<br />
  95%     31<br />
  98%     37<br />
  99%     42<br />
 100%     59 (longest request)<br />
ab -n 500 -c 50 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 100 requests<br />
Completed 200 requests<br />
Completed 300 requests<br />
Completed 400 requests<br />
Completed 500 requests<br />
Finished 500 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   0.887 seconds<br />
Complete requests:      500<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      10353000 bytes<br />
HTML transferred:       10247500 bytes<br />
Requests per second:    563.88 [#/sec] (mean)<br />
Time per request:       88.672 [ms] (mean)<br />
Time per request:       1.773 [ms] (mean across all concurrent requests)<br />
Transfer rate:          11401.98 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    7   8.3      4      31<br />
Processing:    12   78  22.5     78     145<br />
Waiting:        1   71  21.5     73     144<br />
Total:         31   86  20.2     83     174<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     83<br />
  66%     92<br />
  75%     98<br />
  80%    106<br />
  90%    112<br />
  95%    118<br />
  98%    128<br />
  99%    134<br />
 100%    174 (longest request)<br />
ab -n 500 -c 100 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 100 requests<br />
Completed 200 requests<br />
Completed 300 requests<br />
Completed 400 requests<br />
Completed 500 requests<br />
Finished 500 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   0.816 seconds<br />
Complete requests:      500<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      10353000 bytes<br />
HTML transferred:       10247500 bytes<br />
Requests per second:    613.03 [#/sec] (mean)<br />
Time per request:       163.124 [ms] (mean)<br />
Time per request:       1.631 [ms] (mean across all concurrent requests)<br />
Transfer rate:          12395.88 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   12  12.4      6      51<br />
Processing:    28  140  44.8    131     251<br />
Waiting:       13  133  44.8    125     248<br />
Total:         48  152  49.8    139     281<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    139<br />
  66%    161<br />
  75%    166<br />
  80%    189<br />
  90%    244<br />
  95%    262<br />
  98%    271<br />
  99%    276<br />
 100%    281 (longest request)<br />
 <br />
ab -n 1000 -c 1 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 100 requests<br />
Completed 200 requests<br />
Completed 300 requests<br />
Completed 400 requests<br />
Completed 500 requests<br />
Completed 600 requests<br />
Completed 700 requests<br />
Completed 800 requests<br />
Completed 900 requests<br />
Completed 1000 requests<br />
Finished 1000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   1.545 seconds<br />
Complete requests:      1000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      20706000 bytes<br />
HTML transferred:       20495000 bytes<br />
Requests per second:    647.36 [#/sec] (mean)<br />
Time per request:       1.545 [ms] (mean)<br />
Time per request:       1.545 [ms] (mean across all concurrent requests)<br />
Transfer rate:          13090.08 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.3      0       7<br />
Processing:     0    1   1.9      1      20<br />
Waiting:        0    0   0.4      0       8<br />
Total:          0    1   1.9      1      20<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      1<br />
  66%      1<br />
  75%      1<br />
  80%      1<br />
  90%      2<br />
  95%      4<br />
  98%      9<br />
  99%     11<br />
 100%     20 (longest request)<br />
ab -n 1000 -c 10 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 100 requests<br />
Completed 200 requests<br />
Completed 300 requests<br />
Completed 400 requests<br />
Completed 500 requests<br />
Completed 600 requests<br />
Completed 700 requests<br />
Completed 800 requests<br />
Completed 900 requests<br />
Completed 1000 requests<br />
Finished 1000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   1.817 seconds<br />
Complete requests:      1000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      20706000 bytes<br />
HTML transferred:       20495000 bytes<br />
Requests per second:    550.38 [#/sec] (mean)<br />
Time per request:       18.169 [ms] (mean)<br />
Time per request:       1.817 [ms] (mean across all concurrent requests)<br />
Transfer rate:          11129.07 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    5   5.5      3      32<br />
Processing:     1   13  11.6     10      81<br />
Waiting:        0    9   9.2      6      74<br />
Total:          3   18  13.5     15      87<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     15<br />
  66%     19<br />
  75%     23<br />
  80%     24<br />
  90%     32<br />
  95%     38<br />
  98%     70<br />
  99%     82<br />
 100%     87 (longest request)<br />
ab -n 1000 -c 50 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 100 requests<br />
Completed 200 requests<br />
Completed 300 requests<br />
Completed 400 requests<br />
Completed 500 requests<br />
Completed 600 requests<br />
Completed 700 requests<br />
Completed 800 requests<br />
Completed 900 requests<br />
Completed 1000 requests<br />
Finished 1000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   1.509 seconds<br />
Complete requests:      1000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      20706000 bytes<br />
HTML transferred:       20495000 bytes<br />
Requests per second:    662.80 [#/sec] (mean)<br />
Time per request:       75.438 [ms] (mean)<br />
Time per request:       1.509 [ms] (mean across all concurrent requests)<br />
Transfer rate:          13402.27 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    7   6.3      5      34<br />
Processing:    29   66  22.9     59     168<br />
Waiting:       16   59  22.0     53     167<br />
Total:         39   73  23.5     65     182<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     65<br />
  66%     74<br />
  75%     81<br />
  80%     86<br />
  90%     99<br />
  95%    139<br />
  98%    150<br />
  99%    152<br />
 100%    182 (longest request)<br />
ab -n 1000 -c 100 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 100 requests<br />
Completed 200 requests<br />
Completed 300 requests<br />
Completed 400 requests<br />
Completed 500 requests<br />
Completed 600 requests<br />
Completed 700 requests<br />
Completed 800 requests<br />
Completed 900 requests<br />
Completed 1000 requests<br />
Finished 1000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   1.288 seconds<br />
Complete requests:      1000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      20706000 bytes<br />
HTML transferred:       20495000 bytes<br />
Requests per second:    776.37 [#/sec] (mean)<br />
Time per request:       128.805 [ms] (mean)<br />
Time per request:       1.288 [ms] (mean across all concurrent requests)<br />
Transfer rate:          15698.76 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    7   7.0      4      26<br />
Processing:    15  117  23.5    115     195<br />
Waiting:        5  110  23.6    109     166<br />
Total:         29  124  21.2    120     200<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    120<br />
  66%    134<br />
  75%    138<br />
  80%    143<br />
  90%    152<br />
  95%    157<br />
  98%    160<br />
  99%    163<br />
 100%    200 (longest request)<br />
 <br />
ab -n 2000 -c 1 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 200 requests<br />
Completed 400 requests<br />
Completed 600 requests<br />
Completed 800 requests<br />
Completed 1000 requests<br />
Completed 1200 requests<br />
Completed 1400 requests<br />
Completed 1600 requests<br />
Completed 1800 requests<br />
Completed 2000 requests<br />
Finished 2000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   2.625 seconds<br />
Complete requests:      2000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      41412000 bytes<br />
HTML transferred:       40990000 bytes<br />
Requests per second:    761.82 [#/sec] (mean)<br />
Time per request:       1.313 [ms] (mean)<br />
Time per request:       1.313 [ms] (mean across all concurrent requests)<br />
Transfer rate:          15404.53 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.1      0       2<br />
Processing:     0    1   1.8      1      21<br />
Waiting:        0    0   0.5      0       9<br />
Total:          0    1   1.8      1      21<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      1<br />
  66%      1<br />
  75%      1<br />
  80%      1<br />
  90%      2<br />
  95%      3<br />
  98%      9<br />
  99%     10<br />
 100%     21 (longest request)<br />
ab -n 2000 -c 10 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 200 requests<br />
Completed 400 requests<br />
Completed 600 requests<br />
Completed 800 requests<br />
Completed 1000 requests<br />
Completed 1200 requests<br />
Completed 1400 requests<br />
Completed 1600 requests<br />
Completed 1800 requests<br />
Completed 2000 requests<br />
Finished 2000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   2.899 seconds<br />
Complete requests:      2000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      41412000 bytes<br />
HTML transferred:       40990000 bytes<br />
Requests per second:    689.97 [#/sec] (mean)<br />
Time per request:       14.493 [ms] (mean)<br />
Time per request:       1.449 [ms] (mean across all concurrent requests)<br />
Transfer rate:          13951.62 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    4   5.7      3      66<br />
Processing:     1   10   7.6      8      73<br />
Waiting:        0    6   6.9      5      73<br />
Total:          2   14   9.6     11     109<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     11<br />
  66%     15<br />
  75%     17<br />
  80%     19<br />
  90%     24<br />
  95%     31<br />
  98%     43<br />
  99%     57<br />
 100%    109 (longest request)<br />
ab -n 2000 -c 50 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 200 requests<br />
Completed 400 requests<br />
Completed 600 requests<br />
Completed 800 requests<br />
Completed 1000 requests<br />
Completed 1200 requests<br />
Completed 1400 requests<br />
Completed 1600 requests<br />
Completed 1800 requests<br />
Completed 2000 requests<br />
Finished 2000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   2.669 seconds<br />
Complete requests:      2000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      41412000 bytes<br />
HTML transferred:       40990000 bytes<br />
Requests per second:    749.37 [#/sec] (mean)<br />
Time per request:       66.723 [ms] (mean)<br />
Time per request:       1.334 [ms] (mean across all concurrent requests)<br />
Transfer rate:          15152.81 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    6   5.9      4      44<br />
Processing:     9   60  14.9     58     145<br />
Waiting:        2   51  14.2     50     117<br />
Total:         16   65  14.7     64     150<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     64<br />
  66%     70<br />
  75%     74<br />
  80%     77<br />
  90%     84<br />
  95%     88<br />
  98%    105<br />
  99%    109<br />
 100%    150 (longest request)<br />
ab -n 2000 -c 100 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 200 requests<br />
Completed 400 requests<br />
Completed 600 requests<br />
Completed 800 requests<br />
Completed 1000 requests<br />
Completed 1200 requests<br />
Completed 1400 requests<br />
Completed 1600 requests<br />
Completed 1800 requests<br />
Completed 2000 requests<br />
Finished 2000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   2.438 seconds<br />
Complete requests:      2000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      41412000 bytes<br />
HTML transferred:       40990000 bytes<br />
Requests per second:    820.37 [#/sec] (mean)<br />
Time per request:       121.896 [ms] (mean)<br />
Time per request:       1.219 [ms] (mean across all concurrent requests)<br />
Transfer rate:          16588.50 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    7   6.5      6      41<br />
Processing:    12  112  21.7    110     208<br />
Waiting:        2  104  22.0    102     192<br />
Total:         29  119  21.1    116     228<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    116<br />
  66%    125<br />
  75%    131<br />
  80%    135<br />
  90%    149<br />
  95%    158<br />
  98%    165<br />
  99%    169<br />
 100%    228 (longest request)<br />
 <br />
ab -n 5000 -c 1 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 500 requests<br />
Completed 1000 requests<br />
Completed 1500 requests<br />
Completed 2000 requests<br />
Completed 2500 requests<br />
Completed 3000 requests<br />
Completed 3500 requests<br />
Completed 4000 requests<br />
Completed 4500 requests<br />
Completed 5000 requests<br />
Finished 5000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   6.725 seconds<br />
Complete requests:      5000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      103530000 bytes<br />
HTML transferred:       102475000 bytes<br />
Requests per second:    743.55 [#/sec] (mean)<br />
Time per request:       1.345 [ms] (mean)<br />
Time per request:       1.345 [ms] (mean across all concurrent requests)<br />
Transfer rate:          15035.02 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.2      0       9<br />
Processing:     0    1   1.8      1      20<br />
Waiting:        0    0   0.6      0      16<br />
Total:          0    1   1.8      1      20<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      1<br />
  66%      1<br />
  75%      1<br />
  80%      1<br />
  90%      2<br />
  95%      2<br />
  98%      9<br />
  99%     10<br />
 100%     20 (longest request)<br />
ab -n 5000 -c 10 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 500 requests<br />
Completed 1000 requests<br />
Completed 1500 requests<br />
Completed 2000 requests<br />
Completed 2500 requests<br />
Completed 3000 requests<br />
Completed 3500 requests<br />
Completed 4000 requests<br />
Completed 4500 requests<br />
Completed 5000 requests<br />
Finished 5000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   7.301 seconds<br />
Complete requests:      5000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      103530000 bytes<br />
HTML transferred:       102475000 bytes<br />
Requests per second:    684.88 [#/sec] (mean)<br />
Time per request:       14.601 [ms] (mean)<br />
Time per request:       1.460 [ms] (mean across all concurrent requests)<br />
Transfer rate:          13848.70 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    4   4.7      3      40<br />
Processing:     1   10   8.0      8      77<br />
Waiting:        0    6   6.8      4      77<br />
Total:          1   14   9.1     11      77<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     11<br />
  66%     15<br />
  75%     17<br />
  80%     19<br />
  90%     25<br />
  95%     32<br />
  98%     43<br />
  99%     50<br />
 100%     77 (longest request)<br />
ab -n 5000 -c 50 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 500 requests<br />
Completed 1000 requests<br />
Completed 1500 requests<br />
Completed 2000 requests<br />
Completed 2500 requests<br />
Completed 3000 requests<br />
Completed 3500 requests<br />
Completed 4000 requests<br />
Completed 4500 requests<br />
Completed 5000 requests<br />
Finished 5000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   7.087 seconds<br />
Complete requests:      5000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      103530000 bytes<br />
HTML transferred:       102475000 bytes<br />
Requests per second:    705.55 [#/sec] (mean)<br />
Time per request:       70.867 [ms] (mean)<br />
Time per request:       1.417 [ms] (mean across all concurrent requests)<br />
Transfer rate:          14266.74 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   12  12.0      9      84<br />
Processing:     6   58  25.7     53     246<br />
Waiting:        0   42  24.3     39     197<br />
Total:         23   70  25.6     65     246<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     65<br />
  66%     73<br />
  75%     79<br />
  80%     85<br />
  90%    103<br />
  95%    118<br />
  98%    151<br />
  99%    175<br />
 100%    246 (longest request)<br />
ab -n 5000 -c 100 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 500 requests<br />
Completed 1000 requests<br />
Completed 1500 requests<br />
Completed 2000 requests<br />
Completed 2500 requests<br />
Completed 3000 requests<br />
Completed 3500 requests<br />
Completed 4000 requests<br />
Completed 4500 requests<br />
Completed 5000 requests<br />
Finished 5000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   6.407 seconds<br />
Complete requests:      5000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      103530000 bytes<br />
HTML transferred:       102475000 bytes<br />
Requests per second:    780.34 [#/sec] (mean)<br />
Time per request:       128.149 [ms] (mean)<br />
Time per request:       1.281 [ms] (mean across all concurrent requests)<br />
Transfer rate:          15779.02 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   24  18.0     22     121<br />
Processing:    31  102  33.5     97     251<br />
Waiting:        1   74  33.5     67     218<br />
Total:         62  127  28.1    124     255<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    124<br />
  66%    135<br />
  75%    143<br />
  80%    147<br />
  90%    164<br />
  95%    177<br />
  98%    202<br />
  99%    215<br />
 100%    255 (longest request)<br />
 <br />
ab -n 10000 -c 1 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 1000 requests<br />
Completed 2000 requests<br />
Completed 3000 requests<br />
Completed 4000 requests<br />
Completed 5000 requests<br />
Completed 6000 requests<br />
Completed 7000 requests<br />
Completed 8000 requests<br />
Completed 9000 requests<br />
Completed 10000 requests<br />
Finished 10000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   13.816 seconds<br />
Complete requests:      10000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      207060000 bytes<br />
HTML transferred:       204950000 bytes<br />
Requests per second:    723.82 [#/sec] (mean)<br />
Time per request:       1.382 [ms] (mean)<br />
Time per request:       1.382 [ms] (mean across all concurrent requests)<br />
Transfer rate:          14636.24 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.2      0       8<br />
Processing:     0    1   2.1      1      73<br />
Waiting:        0    0   0.6      0      20<br />
Total:          0    1   2.1      1      73<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      1<br />
  66%      1<br />
  75%      1<br />
  80%      1<br />
  90%      2<br />
  95%      3<br />
  98%      9<br />
  99%     10<br />
 100%     73 (longest request)<br />
ab -n 10000 -c 10 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 1000 requests<br />
Completed 2000 requests<br />
Completed 3000 requests<br />
Completed 4000 requests<br />
Completed 5000 requests<br />
Completed 6000 requests<br />
Completed 7000 requests<br />
Completed 8000 requests<br />
Completed 9000 requests<br />
Completed 10000 requests<br />
Finished 10000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   14.122 seconds<br />
Complete requests:      10000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      207060000 bytes<br />
HTML transferred:       204950000 bytes<br />
Requests per second:    708.10 [#/sec] (mean)<br />
Time per request:       14.122 [ms] (mean)<br />
Time per request:       1.412 [ms] (mean across all concurrent requests)<br />
Transfer rate:          14318.34 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    4   4.5      3      41<br />
Processing:     1   10   7.6      8      91<br />
Waiting:        0    6   6.3      4      88<br />
Total:          1   14   8.6     11     132<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     11<br />
  66%     15<br />
  75%     17<br />
  80%     18<br />
  90%     24<br />
  95%     30<br />
  98%     40<br />
  99%     48<br />
 100%    132 (longest request)<br />
ab -n 10000 -c 50 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 1000 requests<br />
Completed 2000 requests<br />
Completed 3000 requests<br />
Completed 4000 requests<br />
Completed 5000 requests<br />
Completed 6000 requests<br />
Completed 7000 requests<br />
Completed 8000 requests<br />
Completed 9000 requests<br />
Completed 10000 requests<br />
Finished 10000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   13.402 seconds<br />
Complete requests:      10000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      207060000 bytes<br />
HTML transferred:       204950000 bytes<br />
Requests per second:    746.18 [#/sec] (mean)<br />
Time per request:       67.008 [ms] (mean)<br />
Time per request:       1.340 [ms] (mean across all concurrent requests)<br />
Transfer rate:          15088.34 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   14  12.1     12      76<br />
Processing:     1   52  24.1     48     166<br />
Waiting:        0   36  23.0     32     155<br />
Total:         12   66  23.7     62     195<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     62<br />
  66%     70<br />
  75%     78<br />
  80%     82<br />
  90%     98<br />
  95%    113<br />
  98%    131<br />
  99%    147<br />
 100%    195 (longest request)<br />
ab -n 10000 -c 100 http://localhost/CS174/hw5/test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
Completed 1000 requests<br />
Completed 2000 requests<br />
Completed 3000 requests<br />
Completed 4000 requests<br />
Completed 5000 requests<br />
Completed 6000 requests<br />
Completed 7000 requests<br />
Completed 8000 requests<br />
Completed 9000 requests<br />
Completed 10000 requests<br />
Finished 10000 requests<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/test.php<br />
Document Length:        20495 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   13.011 seconds<br />
Complete requests:      10000<br />
Failed requests:        0<br />
Write errors:           0<br />
Total transferred:      207060000 bytes<br />
HTML transferred:       204950000 bytes<br />
Requests per second:    768.57 [#/sec] (mean)<br />
Time per request:       130.111 [ms] (mean)<br />
Time per request:       1.301 [ms] (mean across all concurrent requests)<br />
Transfer rate:          15541.11 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   31  23.0     28     141<br />
Processing:    11   98  40.0     92     374<br />
Waiting:        1   66  35.7     61     313<br />
Total:         27  129  35.8    123     374<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    123<br />
  66%    137<br />
  75%    149<br />
  80%    156<br />
  90%    175<br />
  95%    191<br />
  98%    230<br />
  99%    245<br />
 100%    374 (longest request)<br />
                    </div>
                    <div data-role="collapsible" data-collapsed="true">
                        <h3>
                            sql_test.php Benchmark Results (RAW DATA)
                        </h3>
ab -n 100 -c 1 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient).....done<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        714 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   0.755 seconds<br />
Complete requests:      100<br />
Failed requests:        67<br />
   (Connect: 0, Receive: 0, Length: 67, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      94208 bytes<br />
HTML transferred:       71008 bytes<br />
Requests per second:    132.47 [#/sec] (mean)<br />
Time per request:       7.549 [ms] (mean)<br />
Time per request:       7.549 [ms] (mean across all concurrent requests)<br />
Transfer rate:          121.87 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.1      0       0<br />
Processing:     3    7   5.0      6      24<br />
Waiting:        2    7   4.4      5      23<br />
Total:          3    7   5.0      6      24<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      6<br />
  66%      7<br />
  75%      7<br />
  80%      9<br />
  90%     16<br />
  95%     19<br />
  98%     23<br />
  99%     24<br />
 100%     24 (longest request)<br />
ab -n 100 -c 10 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient).....done<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        710 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   0.711 seconds<br />
Complete requests:      100<br />
Failed requests:        58<br />
   (Connect: 0, Receive: 0, Length: 58, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      94184 bytes<br />
HTML transferred:       70984 bytes<br />
Requests per second:    140.57 [#/sec] (mean)<br />
Time per request:       71.139 [ms] (mean)<br />
Time per request:       7.114 [ms] (mean across all concurrent requests)<br />
Transfer rate:          129.29 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    3   8.3      0      35<br />
Processing:     3   67  59.8     43     282<br />
Waiting:        3   66  58.9     43     282<br />
Total:          3   70  61.0     46     282<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     46<br />
  66%     71<br />
  75%     96<br />
  80%    109<br />
  90%    168<br />
  95%    216<br />
  98%    277<br />
  99%    282<br />
 100%    282 (longest request)<br />
ab -n 100 -c 50 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient).....done<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        710 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   0.512 seconds<br />
Complete requests:      100<br />
Failed requests:        50<br />
   (Connect: 0, Receive: 0, Length: 50, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      94152 bytes<br />
HTML transferred:       70952 bytes<br />
Requests per second:    195.29 [#/sec] (mean)<br />
Time per request:       256.034 [ms] (mean)<br />
Time per request:       5.121 [ms] (mean across all concurrent requests)<br />
Transfer rate:          179.56 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   30  26.6     25      62<br />
Processing:    43  168  84.1    165     342<br />
Waiting:       13  162  85.8    164     342<br />
Total:         63  199  69.5    192     396<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    192<br />
  66%    236<br />
  75%    252<br />
  80%    260<br />
  90%    272<br />
  95%    322<br />
  98%    386<br />
  99%    396<br />
 100%    396 (longest request)<br />
ab -n 100 -c 100 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient).....done<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        710 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   0.668 seconds<br />
Complete requests:      100<br />
Failed requests:        52<br />
   (Connect: 0, Receive: 0, Length: 52, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      94188 bytes<br />
HTML transferred:       70988 bytes<br />
Requests per second:    149.59 [#/sec] (mean)<br />
Time per request:       668.500 [ms] (mean)<br />
Time per request:       6.685 [ms] (mean across all concurrent requests)<br />
Transfer rate:          137.59 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:       18   60   7.9     62      67<br />
Processing:    34  353 157.6    385     588<br />
Waiting:       34  352 158.6    385     588<br />
Total:         80  413 161.0    441     653<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    441<br />
  66%    503<br />
  75%    531<br />
  80%    558<br />
  90%    626<br />
  95%    634<br />
  98%    649<br />
  99%    653<br />
 100%    653 (longest request)<br />
ab -n 500 -c 1 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        710 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   2.634 seconds<br />
Complete requests:      500<br />
Failed requests:        286<br />
   (Connect: 0, Receive: 0, Length: 286, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      470936 bytes<br />
HTML transferred:       354936 bytes<br />
Requests per second:    189.85 [#/sec] (mean)<br />
Time per request:       5.267 [ms] (mean)<br />
Time per request:       5.267 [ms] (mean across all concurrent requests)<br />
Transfer rate:          174.63 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.1      0       1<br />
Processing:     2    5   3.7      4      35<br />
Waiting:        0    3   3.2      3      21<br />
Total:          2    5   3.8      4      36<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      4<br />
  66%      4<br />
  75%      5<br />
  80%      5<br />
  90%     11<br />
  95%     12<br />
  98%     18<br />
  99%     20<br />
 100%     36 (longest request)<br />
ab -n 500 -c 10 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        714 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   2.823 seconds<br />
Complete requests:      500<br />
Failed requests:        345<br />
   (Connect: 0, Receive: 0, Length: 345, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      471016 bytes<br />
HTML transferred:       355016 bytes<br />
Requests per second:    177.10 [#/sec] (mean)<br />
Time per request:       56.465 [ms] (mean)<br />
Time per request:       5.646 [ms] (mean across all concurrent requests)<br />
Transfer rate:          162.93 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    4   8.4      0      51<br />
Processing:     2   52  52.5     37     440<br />
Waiting:        0   49  51.6     35     418<br />
Total:          2   56  53.3     40     470<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     40<br />
  66%     51<br />
  75%     60<br />
  80%     65<br />
  90%     98<br />
  95%    186<br />
  98%    230<br />
  99%    263<br />
 100%    470 (longest request)<br />
ab -n 500 -c 50 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        714 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   2.389 seconds<br />
Complete requests:      500<br />
Failed requests:        352<br />
   (Connect: 0, Receive: 0, Length: 352, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      470992 bytes<br />
HTML transferred:       354992 bytes<br />
Requests per second:    209.33 [#/sec] (mean)<br />
Time per request:       238.857 [ms] (mean)<br />
Time per request:       4.777 [ms] (mean across all concurrent requests)<br />
Transfer rate:          192.56 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   14  16.9      9      65<br />
Processing:    33  214  70.4    207     616<br />
Waiting:       10  206  70.4    199     616<br />
Total:         54  228  65.9    216     617<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    216<br />
  66%    236<br />
  75%    253<br />
  80%    262<br />
  90%    299<br />
  95%    365<br />
  98%    416<br />
  99%    462<br />
 100%    617 (longest request)<br />
ab -n 500 -c 100 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        714 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   2.724 seconds<br />
Complete requests:      500<br />
Failed requests:        328<br />
   (Connect: 0, Receive: 0, Length: 328, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      471160 bytes<br />
HTML transferred:       355160 bytes<br />
Requests per second:    183.58 [#/sec] (mean)<br />
Time per request:       544.708 [ms] (mean)<br />
Time per request:       5.447 [ms] (mean across all concurrent requests)<br />
Transfer rate:          168.94 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   18  30.1      0      82<br />
Processing:    44  472 123.8    496     761<br />
Waiting:       12  468 128.7    495     759<br />
Total:         94  490 109.5    500     770<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    500<br />
  66%    526<br />
  75%    545<br />
  80%    562<br />
  90%    602<br />
  95%    640<br />
  98%    672<br />
  99%    745<br />
 100%    770 (longest request)<br />
ab -n 1000 -c 1 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        710 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   4.898 seconds<br />
Complete requests:      1000<br />
Failed requests:        556<br />
   (Connect: 0, Receive: 0, Length: 556, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      942040 bytes<br />
HTML transferred:       710040 bytes<br />
Requests per second:    204.16 [#/sec] (mean)<br />
Time per request:       4.898 [ms] (mean)<br />
Time per request:       4.898 [ms] (mean across all concurrent requests)<br />
Transfer rate:          187.82 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.1      0       3<br />
Processing:     2    5   3.6      3      30<br />
Waiting:        0    3   3.4      3      28<br />
Total:          2    5   3.6      3      30<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      3<br />
  66%      4<br />
  75%      4<br />
  80%      5<br />
  90%     11<br />
  95%     12<br />
  98%     17<br />
  99%     19<br />
 100%     30 (longest request)<br />
ab -n 1000 -c 10 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        714 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   4.971 seconds<br />
Complete requests:      1000<br />
Failed requests:        699<br />
   (Connect: 0, Receive: 0, Length: 699, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      942132 bytes<br />
HTML transferred:       710132 bytes<br />
Requests per second:    201.18 [#/sec] (mean)<br />
Time per request:       49.707 [ms] (mean)<br />
Time per request:       4.971 [ms] (mean across all concurrent requests)<br />
Transfer rate:          185.09 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    7  11.8      0      86<br />
Processing:     3   42  40.8     31     390<br />
Waiting:        0   39  40.6     28     389<br />
Total:          3   49  40.7     38     390<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     38<br />
  66%     47<br />
  75%     57<br />
  80%     63<br />
  90%     88<br />
  95%    132<br />
  98%    192<br />
  99%    210<br />
 100%    390 (longest request)<br />
ab -n 1000 -c 50 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        706 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   4.778 seconds<br />
Complete requests:      1000<br />
Failed requests:        814<br />
   (Connect: 0, Receive: 0, Length: 814, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      942196 bytes<br />
HTML transferred:       710196 bytes<br />
Requests per second:    209.31 [#/sec] (mean)<br />
Time per request:       238.882 [ms] (mean)<br />
Time per request:       4.778 [ms] (mean across all concurrent requests)<br />
Transfer rate:          192.59 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   13  19.2      2      97<br />
Processing:    33  220  67.9    217     571<br />
Waiting:        1  209  67.0    206     571<br />
Total:         53  233  59.4    227     571<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    227<br />
  66%    248<br />
  75%    263<br />
  80%    271<br />
  90%    305<br />
  95%    339<br />
  98%    400<br />
  99%    421<br />
 100%    571 (longest request)<br />
ab -n 1000 -c 100 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        710 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   4.861 seconds<br />
Complete requests:      1000<br />
Failed requests:        551<br />
   (Connect: 0, Receive: 0, Length: 551, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      941924 bytes<br />
HTML transferred:       709924 bytes<br />
Requests per second:    205.70 [#/sec] (mean)<br />
Time per request:       486.146 [ms] (mean)<br />
Time per request:      4.861 [ms] (mean across all concurrent requests)<br />
Transfer rate:          189.21 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   29  41.7     17     287<br />
Processing:    26  437 104.6    438     856<br />
Waiting:        8  412  98.8    417     788<br />
Total:         43  466  96.0    459     892<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    459<br />
  66%    479<br />
  75%    505<br />
  80%    521<br />
  90%    578<br />
  95%    648<br />
  98%    705<br />
  99%    772<br />
 100%    892 (longest request)<br />
ab -n 2000 -c 1 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        714 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   11.027 seconds<br />
Complete requests:      2000<br />
Failed requests:        1420<br />
   (Connect: 0, Receive: 0, Length: 1420, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      1883760 bytes<br />
HTML transferred:       1419760 bytes<br />
Requests per second:    181.38 [#/sec] (mean)<br />
Time per request:       5.513 [ms] (mean)<br />
Time per request:       5.513 [ms] (mean across all concurrent requests)<br />
Transfer rate:          166.83 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.1      0       1<br />
Processing:     2    5   5.7      3      81<br />
Waiting:        0    4   5.3      3      81<br />
Total:          2    5   5.7      3      82<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      3<br />
  66%      4<br />
  75%      4<br />
  80%      5<br />
  90%     11<br />
  95%     14<br />
  98%     23<br />
  99%     29<br />
 100%     82 (longest request)<br />
ab -n 2000 -c 10 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        710 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   9.562 seconds<br />
Complete requests:      2000<br />
Failed requests:        1126<br />
   (Connect: 0, Receive: 0, Length: 1126, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      1883948 bytes<br />
HTML transferred:       1419948 bytes<br />
Requests per second:    209.16 [#/sec] (mean)<br />
Time per request:       47.810 [ms] (mean)<br />
Time per request:       4.781 [ms] (mean across all concurrent requests)<br />
Transfer rate:          192.41 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    8  11.0      1      83<br />
Processing:     2   40  34.5     31     331<br />
Waiting:        0   36  34.3     28     329<br />
Total:          2   47  34.6     38     332<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     38<br />
  66%     48<br />
  75%     57<br />
  80%     62<br />
  90%     85<br />
  95%    112<br />
  98%    162<br />
  99%    193<br />
 100%    332 (longest request)<br />
ab -n 2000 -c 50 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        710 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   9.194 seconds<br />
Complete requests:      2000<br />
Failed requests:        1081<br />
   (Connect: 0, Receive: 0, Length: 1081, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      1883968 bytes<br />
HTML transferred:       1419968 bytes<br />
Requests per second:    217.54 [#/sec] (mean)<br />
Time per request:       229.847 [ms] (mean)<br />
Time per request:       4.597 [ms] (mean across all concurrent requests)<br />
Transfer rate:          200.11 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   46  35.5     45     193<br />
Processing:    13  180  97.5    160     730<br />
Waiting:        2  153  90.2    134     700<br />
Total:         55  226  84.7    207     731<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    207<br />
  66%    239<br />
  75%    265<br />
  80%    283<br />
  90%    336<br />
  95%    403<br />
  98%    450<br />
  99%    513<br />
 100%    731 (longest request)<br />
ab -n 2000 -c 100 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        706 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   8.663 seconds<br />
Complete requests:      2000<br />
Failed requests:        1533<br />
   (Connect: 0, Receive: 0, Length: 1533, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      1883964 bytes<br />
HTML transferred:       1419964 bytes<br />
Requests per second:    230.86 [#/sec] (mean)<br />
Time per request:       433.160 [ms] (mean)<br />
Time per request:       4.332 [ms] (mean across all concurrent requests)<br />
Transfer rate:          212.37 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   93  65.1     96     266<br />
Processing:    15  334 134.2    338     838<br />
Waiting:        5  274 127.0    256     720<br />
Total:         94  428 105.4    429     844<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    429<br />
  66%    462<br />
  75%    485<br />
  80%    499<br />
  90%    550<br />
  95%    604<br />
  98%    680<br />
  99%    725<br />
 100%    844 (longest request)<br />
ab -n 5000 -c 1 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        706 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   26.806 seconds<br />
Complete requests:      5000<br />
Failed requests:        3856<br />
   (Connect: 0, Receive: 0, Length: 3856, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      4709568 bytes<br />
HTML transferred:       3549568 bytes<br />
Requests per second:    186.53 [#/sec] (mean)<br />
Time per request:       5.361 [ms] (mean)<br />
Time per request:       5.361 [ms] (mean across all concurrent requests)<br />
Transfer rate:          171.58 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.1      0       3<br />
Processing:     2    5   4.2      3      51<br />
Waiting:        0    4   3.9      3      50<br />
Total:          2    5   4.2      3      51<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      3<br />
  66%      4<br />
  75%      5<br />
  80%      6<br />
  90%     11<br />
  95%     14<br />
  98%     18<br />
  99%     21<br />
 100%     51 (longest request)<br />
ab -n 5000 -c 10 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        710 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   26.674 seconds<br />
Complete requests:      5000<br />
Failed requests:        2787<br />
   (Connect: 0, Receive: 0, Length: 2787, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      4709980 bytes<br />
HTML transferred:       3549980 bytes<br />
Requests per second:    187.45 [#/sec] (mean)<br />
Time per request:       53.348 [ms] (mean)<br />
Time per request:       5.335 [ms] (mean across all concurrent requests)<br />
Transfer rate:          172.44 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    7  11.1      1      76<br />
Processing:     2   46  45.3     33     497<br />
Waiting:        0   42  45.2     30     492<br />
Total:          2   53  44.7     41     497<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     41<br />
  66%     52<br />
  75%     61<br />
  80%     69<br />
  90%    100<br />
  95%    143<br />
  98%    196<br />
  99%    233<br />
 100%    497 (longest request)<br />
ab -n 5000 -c 50 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        710 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   24.354 seconds<br />
Complete requests:      5000<br />
Failed requests:        2810<br />
   (Connect: 0, Receive: 0, Length: 2810, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      4710412 bytes<br />
HTML transferred:       3550412 bytes<br />
Requests per second:    205.30 [#/sec] (mean)<br />
Time per request:       243.540 [ms] (mean)<br />
Time per request:       4.871 [ms] (mean across all concurrent requests)<br />
Transfer rate:          188.88 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   47  49.4     32     239<br />
Processing:     3  194 104.3    186     781<br />
Waiting:        0  176 101.8    167     752<br />
Total:          3  241  95.5    224     867<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    224<br />
  66%    261<br />
  75%    285<br />
  80%    301<br />
  90%    361<br />
  95%    428<br />
  98%    501<br />
  99%    543<br />
 100%    867 (longest request)<br />
ab -n 5000 -c 100 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        710 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   23.346 seconds<br />
Complete requests:      5000<br />
Failed requests:        2732<br />
   (Connect: 0, Receive: 0, Length: 2732, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      4710072 bytes<br />
HTML transferred:       3550072 bytes<br />
Requests per second:    214.17 [#/sec] (mean)<br />
Time per request:       466.913 [ms] (mean)<br />
Time per request:       4.669 [ms] (mean across all concurrent requests)<br />
Transfer rate:          197.03 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0  113  78.4    112     358<br />
Processing:    24  349 172.1    315    1134<br />
Waiting:        4  290 155.2    250    1019<br />
Total:        113  462 150.6    440    1249<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    440<br />
  66%    504<br />
  75%    540<br />
  80%    573<br />
  90%    663<br />
  95%    745<br />
  98%    839<br />
  99%    892<br />
 100%   1249 (longest request)<br />
ab -n 10000 -c 1 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        714 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   45.751 seconds<br />
Complete requests:      10000<br />
Failed requests:        6985<br />
   (Connect: 0, Receive: 0, Length: 6985, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      9420416 bytes<br />
HTML transferred:       7100416 bytes<br />
Requests per second:    218.57 [#/sec] (mean)<br />
Time per request:       4.575 [ms] (mean)<br />
Time per request:       4.575 [ms] (mean across all concurrent requests)<br />
Transfer rate:          201.08 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.1      0      10<br />
Processing:     2    4   3.4      3      37<br />
Waiting:        0    3   3.2      3      37<br />
Total:          2    5   3.4      3      38<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      3<br />
  66%      3<br />
  75%      4<br />
  80%      4<br />
  90%     10<br />
  95%     11<br />
  98%     16<br />
  99%     18<br />
 100%     38 (longest request)<br />
ab -n 10000 -c 10 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        714 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   46.221 seconds<br />
Complete requests:      10000<br />
Failed requests:        7072<br />
   (Connect: 0, Receive: 0, Length: 7072, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      9419792 bytes<br />
HTML transferred:       7099792 bytes<br />
Requests per second:    216.35 [#/sec] (mean)<br />
Time per request:       46.221 [ms] (mean)<br />
Time per request:       4.622 [ms] (mean across all concurrent requests)<br />
Transfer rate:          199.02 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    7  10.3      1      65<br />
Processing:     2   39  38.2     29     432<br />
Waiting:        0   36  38.0     26     429<br />
Total:          2   46  38.0     37     477<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     37<br />
  66%     45<br />
  75%     52<br />
  80%     58<br />
  90%     81<br />
  95%    120<br />
  98%    173<br />
  99%    205<br />
 100%    477 (longest request)<br />
ab -n 10000 -c 50 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        710 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   44.822 seconds<br />
Complete requests:      10000<br />
Failed requests:        5585<br />
   (Connect: 0, Receive: 0, Length: 5585, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      9420684 bytes<br />
HTML transferred:       7100684 bytes<br />
Requests per second:    223.10 [#/sec] (mean)<br />
Time per request:       224.112 [ms] (mean)<br />
Time per request:       4.482 [ms] (mean across all concurrent requests)<br />
Transfer rate:          205.25 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   50  46.3     40     245<br />
Processing:     3  173  99.7    160     910<br />
Waiting:        0  154  95.2    140     834<br />
Total:          3  222  88.9    208    1021<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    208<br />
  66%    239<br />
  75%    263<br />
  80%    277<br />
  90%    337<br />
  95%    389<br />
  98%    463<br />
  99%    521<br />
 100%   1021 (longest request)<br />
ab -n 10000 -c 100 http://localhost/CS174/hw5/sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/sql_test.php<br />
Document Length:        702 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   43.254 seconds<br />
Complete requests:      10000<br />
Failed requests:        9647<br />
   (Connect: 0, Receive: 0, Length: 9647, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      9321086 bytes<br />
HTML transferred:       7001086 bytes<br />
Requests per second:    231.19 [#/sec] (mean)<br />
Time per request:       432.539 [ms] (mean)<br />
Time per request:       4.325 [ms] (mean across all concurrent requests)<br />
Transfer rate:          210.45 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0  107  77.0     98     451<br />
Processing:     5  324 169.4    319    1178<br />
Waiting:        1  268 153.8    252    1095<br />
Total:          9  430 158.9    422    1221<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    422<br />
  66%    490<br />
  75%    525<br />
  80%    551<br />
  90%    627<br />
  95%    703<br />
  98%    780<br />
  99%    846<br />
 100%   1221 (longest request)<br />
                    </div>
                    <div data-role="collapsible" data-collapsed="true">
                        <h3>
                            memcache_sql_test.php Benchmark Results (RAW DATA)
                        </h3>
ab -n 100 -c 1 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient).....done<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        901 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   0.489 seconds<br />
Complete requests:      100<br />
Failed requests:        95<br />
   (Connect: 0, Receive: 0, Length: 95, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      96122 bytes<br />
HTML transferred:       72922 bytes<br />
Requests per second:    204.68 [#/sec] (mean)<br />
Time per request:       4.886 [ms] (mean)<br />
Time per request:       4.886 [ms] (mean across all concurrent requests)<br />
Transfer rate:          192.13 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.1      0       0<br />
Processing:     2    5   4.3      3      31<br />
Waiting:        0    3   2.7      3      14<br />
Total:          2    5   4.3      3      31<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      3<br />
  66%      4<br />
  75%      4<br />
  80%      5<br />
  90%     10<br />
  95%     13<br />
  98%     22<br />
  99%     31<br />
 100%     31 (longest request)<br />
ab -n 100 -c 10 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient).....done<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        901 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   0.507 seconds<br />
Complete requests:      100<br />
Failed requests:        94<br />
   (Connect: 0, Receive: 0, Length: 94, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      96082 bytes<br />
HTML transferred:       72882 bytes<br />
Requests per second:    197.14 [#/sec] (mean)<br />
Time per request:       50.726 [ms] (mean)<br />
Time per request:       5.073 [ms] (mean across all concurrent requests)<br />
Transfer rate:          184.97 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    6  11.2      0      52<br />
Processing:     4   43  28.5     38     183<br />
Waiting:        0   40  28.4     36     183<br />
Total:          8   49  27.6     44     186<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     44<br />
  66%     52<br />
  75%     56<br />
  80%     60<br />
  90%     75<br />
  95%    106<br />
  98%    148<br />
  99%    186<br />
 100%    186 (longest request)<br />
ab -n 100 -c 50 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient).....done<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        901 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   0.672 seconds<br />
Complete requests:      100<br />
Failed requests:        95<br />
   (Connect: 0, Receive: 0, Length: 95, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      96301 bytes<br />
HTML transferred:       73101 bytes<br />
Requests per second:    148.71 [#/sec] (mean)<br />
Time per request:       336.233 [ms] (mean)<br />
Time per request:       6.725 [ms] (mean across all concurrent requests)<br />
Transfer rate:          139.85 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   25  25.9      4      54<br />
Processing:    19  240 109.3    281     525<br />
Waiting:       15  239 109.9    281     525<br />
Total:         57  265  98.4    284     525<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    284<br />
  66%    301<br />
  75%    311<br />
  80%    317<br />
  90%    344<br />
  95%    436<br />
  98%    523<br />
  99%    525<br />
 100%    525 (longest request)<br />
ab -n 100 -c 100 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient).....done<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        905 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   0.471 seconds<br />
Complete requests:      100<br />
Failed requests:        95<br />
   (Connect: 0, Receive: 0, Length: 95, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      96098 bytes<br />
HTML transferred:       72898 bytes<br />
Requests per second:    212.37 [#/sec] (mean)<br />
Time per request:       470.885 [ms] (mean)<br />
Time per request:       4.709 [ms] (mean across all concurrent requests)<br />
Transfer rate:          199.30 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:       23   43   6.3     42      55<br />
Processing:    24  233 116.0    238     408<br />
Waiting:        7  231 117.9    238     407<br />
Total:         62  276 112.7    279     449<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    279<br />
  66%    349<br />
  75%    377<br />
  80%    392<br />
  90%    430<br />
  95%    440<br />
  98%    447<br />
  99%    449<br />
 100%    449 (longest request)<br />
ab -n 500 -c 1 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        901 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   3.771 seconds<br />
Complete requests:      500<br />
Failed requests:        497<br />
   (Connect: 0, Receive: 0, Length: 497, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      472990 bytes<br />
HTML transferred:       356990 bytes<br />
Requests per second:    132.59 [#/sec] (mean)<br />
Time per request:       7.542 [ms] (mean)<br />
Time per request:       7.542 [ms] (mean across all concurrent requests)<br />
Transfer rate:          122.49 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.1      0       1<br />
Processing:     2    7   5.1      5      42<br />
Waiting:        0    6   5.0      4      34<br />
Total:          2    7   5.1      6      42<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      6<br />
  66%      7<br />
  75%      9<br />
  80%     11<br />
  90%     15<br />
  95%     17<br />
  98%     23<br />
  99%     25<br />
 100%     42 (longest request)<br />
ab -n 500 -c 10 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        901 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   4.619 seconds<br />
Complete requests:      500<br />
Failed requests:        489<br />
   (Connect: 0, Receive: 0, Length: 489, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      474195 bytes<br />
HTML transferred:       358195 bytes<br />
Requests per second:    108.24 [#/sec] (mean)<br />
Time per request:       92.384 [ms] (mean)<br />
Time per request:       9.238 [ms] (mean across all concurrent requests)<br />
Transfer rate:          100.25 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    6  13.4      0      73<br />
Processing:     4   86  64.2     67     417<br />
Waiting:        0   81  63.0     65     417<br />
Total:          4   92  64.6     73     417<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     73<br />
  66%     97<br />
  75%    116<br />
  80%    133<br />
  90%    178<br />
  95%    219<br />
  98%    281<br />
  99%    346<br />
 100%    417 (longest request)<br />
ab -n 500 -c 50 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        901 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   4.530 seconds<br />
Complete requests:      500<br />
Failed requests:        487<br />
   (Connect: 0, Receive: 0, Length: 487, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      477001 bytes<br />
HTML transferred:       361001 bytes<br />
Requests per second:    110.37 [#/sec] (mean)<br />
Time per request:       453.040 [ms] (mean)<br />
Time per request:       9.061 [ms] (mean across all concurrent requests)<br />
Transfer rate:          102.82 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   31  43.2     17     239<br />
Processing:    22  411 161.1    421    1018<br />
Waiting:        8  382 149.0    399     923<br />
Total:         43  442 148.1    451    1018<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    451<br />
  66%    502<br />
  75%    543<br />
  80%    568<br />
  90%    622<br />
  95%    665<br />
  98%    746<br />
  99%    826<br />
 100%   1018 (longest request)<br />
ab -n 500 -c 100 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        897 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   2.309 seconds<br />
Complete requests:      500<br />
Failed requests:        494<br />
   (Connect: 0, Receive: 0, Length: 494, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      475751 bytes<br />
HTML transferred:       359751 bytes<br />
Requests per second:    216.55 [#/sec] (mean)<br />
Time per request:       461.785 [ms] (mean)<br />
Time per request:       4.618 [ms] (mean across all concurrent requests)<br />
Transfer rate:          201.22 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   51  52.2     37     171<br />
Processing:    78  378 120.9    400     792<br />
Waiting:       58  356 118.4    373     785<br />
Total:        185  430  89.5    445     793<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    445<br />
  66%    471<br />
  75%    481<br />
  80%    486<br />
  90%    522<br />
  95%    550<br />
  98%    608<br />
  99%    689<br />
 100%    793 (longest request)<br />
ab -n 1000 -c 1 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        897 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   6.842 seconds<br />
Complete requests:      1000<br />
Failed requests:        990<br />
   (Connect: 0, Receive: 0, Length: 990, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      948319 bytes<br />
HTML transferred:       716319 bytes<br />
Requests per second:    146.15 [#/sec] (mean)<br />
Time per request:       6.842 [ms] (mean)<br />
Time per request:       6.842 [ms] (mean across all concurrent requests)<br />
Transfer rate:          135.35 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.1      0       2<br />
Processing:     2    7   5.6      4      48<br />
Waiting:        0    5   5.2      3      39<br />
Total:          2    7   5.6      4      48<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      4<br />
  66%      6<br />
  75%      9<br />
  80%     10<br />
  90%     14<br />
  95%     17<br />
  98%     25<br />
  99%     28<br />
 100%     48 (longest request)<br />
ab -n 1000 -c 10 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        897 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   5.575 seconds<br />
Complete requests:      1000<br />
Failed requests:        991<br />
   (Connect: 0, Receive: 0, Length: 991, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      947224 bytes<br />
HTML transferred:       715224 bytes<br />
Requests per second:    179.38 [#/sec] (mean)<br />
Time per request:       55.746 [ms] (mean)<br />
Time per request:       5.575 [ms] (mean across all concurrent requests)<br />
Transfer rate:          165.93 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    7  11.6      0      63<br />
Processing:     3   48  40.4     37     283<br />
Waiting:        0   45  40.5     34     283<br />
Total:          3   55  41.0     45     284<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     45<br />
  66%     57<br />
  75%     66<br />
  80%     72<br />
  90%    102<br />
  95%    146<br />
  98%    195<br />
  99%    215<br />
 100%    284 (longest request)<br />
ab -n 1000 -c 50 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        905 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   6.089 seconds<br />
Complete requests:      1000<br />
Failed requests:        983<br />
   (Connect: 0, Receive: 0, Length: 983, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      952222 bytes<br />
HTML transferred:       720222 bytes<br />
Requests per second:    164.23 [#/sec] (mean)<br />
Time per request:       304.459 [ms] (mean)<br />
Time per request:       6.089 [ms] (mean across all concurrent requests)<br />
Transfer rate:          152.71 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   23  33.2      5     206<br />
Processing:    17  277 143.4    258     957<br />
Waiting:        2  260 143.5    229     949<br />
Total:         63  300 130.0    269     957<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    269<br />
  66%    332<br />
  75%    368<br />
  80%    395<br />
  90%    480<br />
  95%    544<br />
  98%    644<br />
  99%    698<br />
 100%    957 (longest request)<br />
ab -n 1000 -c 100 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        901 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   7.400 seconds<br />
Complete requests:      1000<br />
Failed requests:        948<br />
   (Connect: 0, Receive: 0, Length: 948, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      962047 bytes<br />
HTML transferred:       730047 bytes<br />
Requests per second:    135.14 [#/sec] (mean)<br />
Time per request:       739.991 [ms] (mean)<br />
Time per request:       7.400 [ms] (mean across all concurrent requests)<br />
Transfer rate:          126.96 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0  134  96.9    136     431<br />
Processing:    21  596 365.3    536    2034<br />
Waiting:        2  524 352.8    452    2015<br />
Total:        104  729 355.8    680    2137<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    680<br />
  66%    814<br />
  75%    917<br />
  80%   1042<br />
  90%   1255<br />
  95%   1393<br />
  98%   1602<br />
  99%   1716<br />
 100%   2137 (longest request)<br />
ab -n 2000 -c 1 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        905 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   11.845 seconds<br />
Complete requests:      2000<br />
Failed requests:        1952<br />
   (Connect: 0, Receive: 0, Length: 1952, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      1916263 bytes<br />
HTML transferred:       1452263 bytes<br />
Requests per second:    168.85 [#/sec] (mean)<br />
Time per request:       5.922 [ms] (mean)<br />
Time per request:       5.922 [ms] (mean across all concurrent requests)<br />
Transfer rate:          157.99 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.3      0       9<br />
Processing:     2    6   4.4      4      36<br />
Waiting:        0    4   4.2      3      29<br />
Total:          2    6   4.4      4      37<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      4<br />
  66%      5<br />
  75%      7<br />
  80%      9<br />
  90%     12<br />
  95%     16<br />
  98%     19<br />
  99%     22<br />
 100%     37 (longest request)<br />
ab -n 2000 -c 10 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        905 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   12.955 seconds<br />
Complete requests:      2000<br />
Failed requests:        1962<br />
   (Connect: 0, Receive: 0, Length: 1962, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      1906773 bytes<br />
HTML transferred:       1442773 bytes<br />
Requests per second:    154.38 [#/sec] (mean)<br />
Time per request:       64.777 [ms] (mean)<br />
Time per request:       6.478 [ms] (mean across all concurrent requests)<br />
Transfer rate:          143.73 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    6  11.5      0     115<br />
Processing:     3   58  57.0     42     443<br />
Waiting:        0   54  56.4     38     443<br />
Total:          3   64  57.7     47     443<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     47<br />
  66%     61<br />
  75%     75<br />
  80%     89<br />
  90%    135<br />
  95%    184<br />
  98%    243<br />
  99%    286<br />
 100%    443 (longest request)<br />
ab -n 2000 -c 50 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        905 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   10.690 seconds<br />
Complete requests:      2000<br />
Failed requests:        1964<br />
   (Connect: 0, Receive: 0, Length: 1964, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      1906223 bytes<br />
HTML transferred:       1442223 bytes<br />
Requests per second:    187.08 [#/sec] (mean)<br />
Time per request:       267.258 [ms] (mean)<br />
Time per request:       5.345 [ms] (mean across all concurrent requests)<br />
Transfer rate:          174.13 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   45  50.1     30     250<br />
Processing:    15  219 122.6    208     752<br />
Waiting:        2  193 115.2    177     745<br />
Total:         38  264 117.8    247     788<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    247<br />
  66%    298<br />
  75%    331<br />
  80%    350<br />
  90%    431<br />
  95%    483<br />
  98%    553<br />
  99%    604<br />
 100%    788 (longest request)<br />
ab -n 2000 -c 100 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        905 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   9.140 seconds<br />
Complete requests:      2000<br />
Failed requests:        1956<br />
   (Connect: 0, Receive: 0, Length: 1956, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      1901378 bytes<br />
HTML transferred:       1437378 bytes<br />
Requests per second:    218.82 [#/sec] (mean)<br />
Time per request:       457.005 [ms] (mean)<br />
Time per request:       4.570 [ms] (mean across all concurrent requests)<br />
Transfer rate:          203.15 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   84  67.0     80     350<br />
Processing:    13  367 159.0    371    1299<br />
Waiting:        2  315 142.8    299    1159<br />
Total:         60  451 134.4    439    1299<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    439<br />
  66%    489<br />
  75%    515<br />
  80%    531<br />
  90%    610<br />
  95%    691<br />
  98%    763<br />
  99%    833<br />
 100%   1299 (longest request)<br />
ab -n 5000 -c 1 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        897 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   22.968 seconds<br />
Complete requests:      5000<br />
Failed requests:        4900<br />
   (Connect: 0, Receive: 0, Length: 4900, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      4791767 bytes<br />
HTML transferred:       3631767 bytes<br />
Requests per second:    217.69 [#/sec] (mean)<br />
Time per request:       4.594 [ms] (mean)<br />
Time per request:       4.594 [ms] (mean across all concurrent requests)<br />
Transfer rate:          203.74 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.1      0       3<br />
Processing:     2    4   3.1      3      32<br />
Waiting:        0    3   3.0      3      29<br />
Total:          2    5   3.1      3      33<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      3<br />
  66%      4<br />
  75%      4<br />
  80%      4<br />
  90%     10<br />
  95%     11<br />
  98%     13<br />
  99%     17<br />
 100%     33 (longest request)<br />
ab -n 5000 -c 10 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        901 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   23.330 seconds<br />
Complete requests:      5000<br />
Failed requests:        4878<br />
   (Connect: 0, Receive: 0, Length: 4878, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      4765119 bytes<br />
HTML transferred:       3605119 bytes<br />
Requests per second:    214.32 [#/sec] (mean)<br />
Time per request:       46.660 [ms] (mean)<br />
Time per request:       4.666 [ms] (mean across all concurrent requests)<br />
Transfer rate:          199.46 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    8  10.9      1      94<br />
Processing:     2   39  37.3     29     422<br />
Waiting:        0   35  36.9     26     415<br />
Total:          2   46  37.3     39     449<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     39<br />
  66%     45<br />
  75%     51<br />
  80%     56<br />
  90%     77<br />
  95%    122<br />
  98%    173<br />
  99%    206<br />
 100%    449 (longest request)<br />
ab -n 5000 -c 50 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        901 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   21.631 seconds<br />
Complete requests:      5000<br />
Failed requests:        4898<br />
   (Connect: 0, Receive: 0, Length: 4898, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      4754885 bytes<br />
HTML transferred:       3594885 bytes<br />
Requests per second:    231.15 [#/sec] (mean)<br />
Time per request:       216.314 [ms] (mean)<br />
Time per request:       4.326 [ms] (mean across all concurrent requests)<br />
Transfer rate:          214.66 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   48  45.2     40     225<br />
Processing:    13  167  97.9    151     714<br />
Waiting:        1  148  92.3    133     713<br />
Total:         16  215  86.4    204     714<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    204<br />
  66%    237<br />
  75%    256<br />
  80%    272<br />
  90%    328<br />
  95%    376<br />
  98%    444<br />
  99%    490<br />
 100%    714 (longest request)<br />
ab -n 5000 -c 100 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        901 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   30.691 seconds<br />
Complete requests:      5000<br />
Failed requests:        4753<br />
   (Connect: 0, Receive: 0, Length: 4753, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      4692602 bytes<br />
HTML transferred:       3532602 bytes<br />
Requests per second:    162.92 [#/sec] (mean)<br />
Time per request:       613.812 [ms] (mean)<br />
Time per request:       6.138 [ms] (mean across all concurrent requests)<br />
Transfer rate:          149.32 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0  121 103.1    101     602<br />
Processing:     2  486 308.0    426    2349<br />
Waiting:        0  414 280.0    354    2241<br />
Total:         10  607 296.3    569    2352<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    569<br />
  66%    693<br />
  75%    784<br />
  80%    837<br />
  90%   1004<br />
  95%   1123<br />
  98%   1290<br />
  99%   1428<br />
 100%   2352 (longest request)<br />
ab -n 10000 -c 1 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        901 bytes<br />
<br />
Concurrency Level:      1<br />
Time taken for tests:   58.380 seconds<br />
Complete requests:      10000<br />
Failed requests:        9676<br />
   (Connect: 0, Receive: 0, Length: 9676, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      9561015 bytes<br />
HTML transferred:       7241015 bytes<br />
Requests per second:    171.29 [#/sec] (mean)<br />
Time per request:       5.838 [ms] (mean)<br />
Time per request:       5.838 [ms] (mean across all concurrent requests)<br />
Transfer rate:          159.93 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    0   0.2      0      10<br />
Processing:     2    6   4.6      4      67<br />
Waiting:        0    4   4.5      3      57<br />
Total:          2    6   4.6      4      67<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%      4<br />
  66%      5<br />
  75%      6<br />
  80%      8<br />
  90%     11<br />
  95%     15<br />
  98%     19<br />
  99%     24<br />
 100%     67 (longest request)<br />
ab -n 10000 -c 10 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        905 bytes<br />
<br />
Concurrency Level:      10<br />
Time taken for tests:   46.802 seconds<br />
Complete requests:      10000<br />
Failed requests:        9949<br />
   (Connect: 0, Receive: 0, Length: 9949, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      9455244 bytes<br />
HTML transferred:       7135244 bytes<br />
Requests per second:    213.67 [#/sec] (mean)<br />
Time per request:       46.802 [ms] (mean)<br />
Time per request:       4.680 [ms] (mean across all concurrent requests)<br />
Transfer rate:          197.29 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0    6   9.9      0      70<br />
Processing:     2   40  38.9     30     444<br />
Waiting:        0   37  38.6     27     443<br />
Total:          2   47  38.7     37     444<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%     37<br />
  66%     45<br />
  75%     52<br />
  80%     58<br />
  90%     84<br />
  95%    129<br />
  98%    178<br />
  99%    207<br />
 100%    444 (longest request)<br />
ab -n 10000 -c 50 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        897 bytes<br />
<br />
Concurrency Level:      50<br />
Time taken for tests:   44.754 seconds<br />
Complete requests:      10000<br />
Failed requests:        9890<br />
   (Connect: 0, Receive: 0, Length: 9890, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      9509138 bytes<br />
HTML transferred:       7189138 bytes<br />
Requests per second:    223.45 [#/sec] (mean)<br />
Time per request:       223.768 [ms] (mean)<br />
Time per request:       4.475 [ms] (mean across all concurrent requests)<br />
Transfer rate:          207.50 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0   45  46.2     32     267<br />
Processing:     3  177 101.4    167     814<br />
Waiting:        0  159  96.8    150     807<br />
Total:          3  222  91.2    210     828<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    210<br />
  66%    243<br />
  75%    270<br />
  80%    287<br />
  90%    337<br />
  95%    390<br />
  98%    450<br />
  99%    513<br />
 100%    828 (longest request)<br />
ab -n 10000 -c 100 http://localhost/CS174/hw5/memcache_sql_test.php<br />
This is ApacheBench, Version 2.3 &#60;&#36;Revision: 1430300 &#62;<br />
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/<br />
Licensed to The Apache Software Foundation, http://www.apache.org/<br />
<br />
Benchmarking localhost (be patient)<br />
<br />
<br />
Server Software:        Apache/2.4.4<br />
Server Hostname:        localhost<br />
Server Port:            80<br />
<br />
Document Path:          /CS174/hw5/memcache_sql_test.php<br />
Document Length:        905 bytes<br />
<br />
Concurrency Level:      100<br />
Time taken for tests:   45.460 seconds<br />
Complete requests:      10000<br />
Failed requests:        9751<br />
   (Connect: 0, Receive: 0, Length: 9751, Exceptions: 0)<br />
Write errors:           0<br />
Total transferred:      9443275 bytes<br />
HTML transferred:       7123275 bytes<br />
Requests per second:    219.98 [#/sec] (mean)<br />
Time per request:       454.597 [ms] (mean)<br />
Time per request:       4.546 [ms] (mean across all concurrent requests)<br />
Transfer rate:          202.86 [Kbytes/sec] received<br />
<br />
Connection Times (ms)<br />
              min  mean[+/-sd] median   max<br />
Connect:        0  116  84.5    108     485<br />
Processing:     1  336 179.2    319    1305<br />
Waiting:        0  280 162.2    257    1152<br />
Total:         11  452 169.3    446    1430<br />
<br />
Percentage of the requests served within a certain time (ms)<br />
  50%    446<br />
  66%    506<br />
  75%    556<br />
  80%    589<br />
  90%    664<br />
  95%    736<br />
  98%    827<br />
  99%    880<br />
 100%   1430 (longest request)<br />
                    </div>
                    <div data-role="collapsible" data-collapsed="true">
                        <h3>
                            test: html and php Results Analysis
                        </h3>
                        <p>
                            At 10,0000 requests and concurrency level of 100,
                            the number of requests per second of the html
                            page is larger than the php file. Below is the
                            benchmark of test.html:
                        </p>
                            <pre>
Requests per second: 1373.72 [#/sec] (mean)
Time per request: 72.795 [ms] (mean)
Time per request: 0.728 [ms] (mean across all concurrent requests)
Transfer rate: 27876.91 [Kbytes/sec] received
                            </pre>
                        <p>
                            Below is the benchmark of test.php:
                        </p>
                            <pre>
Requests per second: 768.57 [#/sec] (mean)
Time per request: 130.111 [ms] (mean)
Time per request: 1.301 [ms] (mean across all concurrent requests)
Transfer rate: 15541.11 [Kbytes/sec] received
                            </pre>
                        <p>
                            A little more than 600 requests were served
                            with the html file than the php file. This comes
                            to show that static content can be delivered
                            faster than dynamic content.
                        </p>
                        <p>
                            In the benchmarking done for test.html, the
                            requests per second when the requests were below
                            2,000 were within the range of 500-700 requests
                            per second. However, at 2,000 requests and
                            50 requests served concurrently, the number
                            of requests per second jumped dramatically to
                            1,369.71 requests per second. Afterwards, the
                            requests per second stayed within the range of
                            1,100-1,500. This may be due to CPU being freed
                            on my local machine as I had other processes
                            running during the benchmarking. Another reason
                            may be due to the fact that I am running
                            the benchmarking on a Virtual Machine with
                            1024 MB of memory.
                        </p>
                        <p>
                            In the benchmarking done for test.php, the
                            requests per second averaged between 500-800
                            requests per second throughout all the tests.
                        </p>
                    </div>
                    <div data-role="collapsible" data-collapsed="true">
                        <h3>
                            sql_test and memcache_sql_test Results Analysis
                        </h3>
                        <p>
                            Surprising results came from the benchmarking
                            between sql_test and memcache_sql_test. At
                            10,000 requests with 100 requests served
                            concurrently, the memcache_sql_test served fewer
                            requests than the sql_test.
                        </p>
                        <p>
                            Below are the benchmark results for sql_test.php:
                        </p>
                            <pre>
Requests per second: 231.19 [#/sec] (mean)
Time per request: 432.539 [ms] (mean)
Time per request: 4.325 [ms] (mean across all concurrent requests)
Transfer rate: 210.45 [Kbytes/sec] received
                            </pre>
                        <p>
                            Below are the benchmark results for
                            memcache_sql_test.php:
                        </p>
                            <pre>
Requests per second: 219.98 [#/sec] (mean)
Time per request: 454.597 [ms] (mean)
Time per request: 4.546 [ms] (mean across all concurrent requests)
Transfer rate: 202.86 [Kbytes/sec] received
                            </pre>
                        <p>
                            The requests per second ranged between 100-250,
                            but fluctuated between the two; there are times
                            at which more requests were served with
                            memcache_sql_test but other times when
                            sql_test served more requests.
                        </p>
                        <p>
                            One possible reason for the closeness in numbers
                            is that the result from the MySQL query is stored
                            in memcache and not a parsed result. In the
                            memcache_sql_test file, the only thing that is not
                            done is the sql query to inner join the two tables
                            USER and USER_POST and retrieve the last 15
                            tweets. If I wanted to produce faster results,
                            I could have concatenated the tweets to a String,
                            store the String object in memcache, and return
                            that instead of having to parse the MySQL result.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
EOF;
        echo $html_code;
    }
}
?>