<?php
/** 
 * The base view for the mobile site.
 * Authors: Nicolas Seto, Dora Do
 */
interface View {
    /** 
     * Display relevant advertisement information.
     */
    function display();
}
?>