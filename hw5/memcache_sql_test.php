<?php
$db_host = 'localhost';
$db_user = 'root';
$db_pass = 'password';
$db_name = 'SQL_TEST';

$con = mysqli_connect($db_host, $db_user, $db_pass, $db_name);

if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') ' . 
        mysqli_connect_error());
}

echo "Successfully connected to MySQL and selected SQL_TEST...\n";

$user_query = "SELECT NAME FROM `USER` ORDER BY RAND() LIMIT 1;";

$memcache = new Memcache();
$memcache->addServer("localhost", 10000, true, 1, 1, 5);

for ($i = 0; $i < 3; $i++) {
    $result = mysqli_query($con, $user_query);
    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
    $user = $row['NAME'];
    if ($result = $memcache->get($user)) {
    
    } else {
        $query = "SELECT `TWEETS` FROM `USER` INNER JOIN `USER_POST` ON
            `USER`.`ID` = `USER_POST`.`USER_ID` and `USER`.`NAME` = '{$user}'
            ORDER BY `AT_WHAT_TIME` DESC LIMIT 15;";
        $result = mysqli_query($con, $query);
        $memcache->set($user, $result, null, 60);
    }
    echo "Name: $user\nTweets:\n";
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
        echo "{$row['TWEETS']}\n";
    }
}



mysqli_close($con);
?>
