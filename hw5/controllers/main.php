<?php
require_once('controller.php');
require_once($conf['doc_root'].'views/main.php');

/**
 * The main controller class for the mobile site, which manages the site and
 * determines what the users will see.
 * Authors: Nicolas Seto, Dora Do
 */
class MainController implements Controller {
    private $view;
    
    function __construct() {
        $this->view = new MainView();
    }
    
    /**
     * Process the request based on request.
     */
    public function process() {
        $this->view->display();
    }
}
?>