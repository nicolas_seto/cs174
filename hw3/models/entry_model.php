﻿<?php
require_once('model.php');
/** The main model class for Looney Limericks. Validates limericks before
 *  saving them to the database. Retrieves limericks and the top ten rated
 *  or most recent ten.
 *  Authors: Nicolas Seto, Dora Do
 */
class Model implements Model_ {
    private $timestamp;
    
    function __construct() {
        $this->timestamp = null;
    }
    
    /** Retrieves a random limerick and returns it to the controller. 
     *  Updates the time. Puts the relevant poem information into an array
     *  and returns.
     */
    public function getRandomPoem() {
        global $conf;
        $con = mysqli_connect($conf['db_host'], $conf['db_user'], 
               $conf['db_pass'], $conf['db_name']);
        $query = "SELECT COUNT(*) AS Num FROM Poems;";
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        /* Compute random value between 1 and total */
        $random = rand(1, $row['Num']);
        $query = "SELECT * FROM Poems WHERE PoemNum={$random};";
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        mysqli_close($con);
        return $row;
    }
    /** Retrieves the specified poem via the unique identifier of the poem.
     *
     */
    public function getPoem($poem_num) {
        global $conf;
        $con = mysqli_connect($conf['db_host'], $conf['db_user'], 
               $conf['db_pass'], $conf['db_name']);
        $query = "SELECT * FROM Poems WHERE PoemNum={$poem_num};";
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        mysqli_close($con);
        return $row;
    }
    /** Save data into database. Validate rhyme scheme; if invalid, gives an
     *  error message. Also cleans the data before saving.
     */
    public function submitPoem($tuple) {
        global $conf;
        $title = $tuple[0];
        $author = $tuple[1];
        $limerick = $tuple[2];
        
        /* Place the last words of each line into an array */
        $lines = explode("\n", $limerick);
        foreach ($lines as $line) {
            $words = explode(" ", $line);
            $rhymes[] = $words[count($words) - 1];
        }
        /* Take the metaphone of each last word */
        for ($i = 0; $i < count($rhymes); $i++) {
            $rhymes[$i] = metaphone($rhymes[$i]);
        }
        $a = max(strlen($rhymes[0]), strlen($rhymes[1]), strlen($rhymes[4]));
        $b = max(strlen($rhymes[2]), strlen($rhymes[3]));
        $MAX_ERROR = 1/3;
        /* Compute the levenshtein distances and compare them with max 
         * lengths of the metaphones.
         */
        $distance[0] = levenshtein($rhymes[0], $rhymes[1]) / $a;
        $distance[1] = levenshtein($rhymes[0], $rhymes[4]) / $a;
        $distance[2] = levenshtein($rhymes[1], $rhymes[4]) / $a;
        $distance[3] = levenshtein($rhymes[2], $rhymes[3]) / $b;
        $distance[4] = 3/4; #MAX_ERROR 75%
        
        if ($distance[4] == max($distance)) {
                $con =
                    mysqli_connect($conf['db_host'], $conf['db_user'], 
                        $conf['db_pass'], $conf['db_name']);
                /* Escape the strings */
                $title = mysqli_real_escape_string($con, $title);
                $author = mysqli_real_escape_string($con, $author);
                $limerick = mysqli_real_escape_string($con, $limerick);
                $time = time();
                
                $query = "INSERT INTO Poems(Title, 
                Author, PoemContent, SubmissionTime) 
                VALUES('{$title}', '{$author}', '{$limerick}', {$time});";
                if ($result = mysqli_query($con, $query)) {
                    $query = "SELECT PoemNum FROM Poems 
                        WHERE SubmissionTime={$time};";
                    $result = mysqli_query($con, $query);
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    mysqli_close($con); #Close the connection
                    
                    return $row['PoemNum'];
                } else {
                    mysqli_close($con);
                }
        } else {
            return 0;
        }
    }
    /** Retrieves the featured poem. Gets the tuple with the latest 
     *  timestamp,
     *  checks the timestamp with the current time, and if the difference is
     *  10 minutes or more, select a new featured poem. Otherwise, return
     *  the tuple.
     */
    public function getFeatured() {
        global $conf;
        $con = mysqli_connect($conf['db_host'], $conf['db_user'], 
            $conf['db_pass'], $conf['db_name']);
        $query = "SELECT * FROM Poems WHERE IsFeatured = TRUE;";
        $result = mysqli_query($con, $query);
        /* If no tuple is marked TRUE, set first Featured limerick */
        if (null == ($row = mysqli_fetch_array($result, MYSQLI_ASSOC))) {
            /* Get total number of poems */
            $query = "SELECT COUNT(*) AS Num FROM Poems;";
            $result = mysqli_query($con, $query);
            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            /* Compute random value between 1 and total */
            $random = rand(1, $row['Num']);
            $time = time();
            /* Update the tuple and set Featured to true and set time*/
            $query = "UPDATE Poems SET 
                IsFeatured=TRUE, FeaturedTimestamp={$time} 
                WHERE PoemNum={$random};";
            mysqli_query($con, $query);
            mysqli_close($con);
            return $this->getPoem($random);
        } else {
            /* Check whether featured time has been longer than 10 min */
            $time_difference = (time() - $row['FeaturedTimestamp']) / 60;
            if ($time_difference > 10) {
                /* A new featured limerick must be selected */
                /* Get total number of poems */
                $query = "SELECT COUNT(*) AS Num FROM Poems;";
                $result = mysqli_query($con, $query);
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                /* Compute random value between 1 and total */
                $random = rand(1, $row['Num']);
                $time = time();
                /* Update the tuple and set Featured to true and set time*/
                $query = "UPDATE Poems SET
                    IsFeatured=FALSE
                    WHERE IsFeatured=TRUE;
                    UPDATE Poems SET 
                    IsFeatured=TRUE, FeaturedTimestamp={$time} 
                    WHERE PoemNum={$random};";
                mysqli_multi_query($con, $query);
                /* Get rid of results from query */
                while (mysqli_more_results($con) && 
                    mysqli_next_result($con));
                mysqli_close($con);
                return $this->getPoem($random);
                
            } else {
                /* Return the featured limerick */
                mysqli_close($con);
                return $row;
            }
        }
    }
    /** When the user inputs a new rating, the entire average is
     *  recalculated and the user's rating is updated as well.
     */
    public function updateRating($pID, $newYourRating) {
        global $conf;
        $con = mysqli_connect($conf['db_host'], $conf['db_user'], 
               $conf['db_pass'], $conf['db_name']);
        $query = "SELECT UserRating, NumRatings FROM Poems 
            WHERE PoemNum={$pID};";
        $result = mysqli_query($con, $query);
        $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
        $newNumRatings = $row['NumRatings'] + 1;
        $newUserRating = (($row['UserRating'] * $row['NumRatings']) + 
            $newYourRating) / $newNumRatings;
        $query = "UPDATE Poems SET UserRating={$newUserRating}, 
            YourRating={$newYourRating}, NumRatings={$newNumRatings}
            WHERE PoemNum={$pID};";
        mysqli_query($con, $query);
        mysqli_close($con);
        $_SESSION[$pID] = $newYourRating;
    }
    /** Retrieves the top 10 rated poems and returns it.
     *
     */
    public function retrieveTopTenRated() {
        global $conf;
        $con = mysqli_connect($conf['db_host'], $conf['db_user'], 
               $conf['db_pass'], $conf['db_name']);
        $query = "SELECT * FROM Poems ORDER BY UserRating DESC, Title ASC 
            LIMIT 0, 10;";
        $result = mysqli_query($con, $query);
        while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
            $rows[] = $row;
        }
        mysqli_close($con);
        return $rows;
    }
    /** Retrieves the 10 most recently submitted poems.
     *
     */
    public function retrieveTenRecent() {
        global $conf;
        $con = mysqli_connect($conf['db_host'], $conf['db_user'], 
               $conf['db_pass'], $conf['db_name']);
        $query = "SELECT * FROM Poems ORDER BY SubmissionTime DESC
            LIMIT 0, 10;";
        $result = mysqli_query($con, $query);
        while ($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
            $rows[] = $row;
        }
        mysqli_close($con);
        return $rows;
    }
}

?>