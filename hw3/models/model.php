﻿<?php
/** The model in the MVC pattern. Has direct access to database and
 *  manipulates the data.
 *  Authors: Nicolas Seto, Dora Do
 */
interface Model_ {
    /** Retrieves a random poem and returns to the controller. Updates the
     *  time. Puts the relevant poem information into an array and returns.
     */
    function getRandomPoem();
    /** Retrieves the specified poem via the unique identifier of the poem.
     *
     */
    function getPoem($poem_num);
    /** Save data into database. Validate rhyme scheme; if invalid, gives an
     *  error message. Also cleans the data before saving.
     */
    function submitPoem($tuple);
    /** Retrieves the featured poem. Gets the tuple with the latest timestamp,
     *  checks the timestamp with the current time, and if the difference is
     *  10 minutes or more, select a new featured poem. Otherwise, return
     *  the tuple.
     */
    function getFeatured();
    /** When the user inputs a new rating, the entire average is
     *  recalculated.
     */
    function updateRating($pID, $newYourRating);
    /** Retrieves the top 10 rated poems and returns it.
     *
     */
    function retrieveTopTenRated();
    /** Retrieves the 10 most recently submitted poems.
     *
     */
    function retrieveTenRecent();
}
?>