<?php
/* Authors: Nicolas Seto, Dora Do */
# Default values may be overridden per host
# Change the baseURL, ending with a forward slash, if necessary
# If the index.php is located in Hw3 in the document root, enter
# as the following example: 'http://localhost/Hw3/'
$conf['baseURL'] = 'http://localhost/';
# Please specify the path to your Document Root.
# Also, if you do not deploy from the top-most level directory of the 
# Document Root, please make the necessary changes, ending with a forward 
# slash
# (e.g. "C:/your/path/Hw3/")
$conf['DOC_ROOT'] = '/opt/lampp/htdocs/';
$conf['db_host'] = 'localhost';
$conf['db_user'] = 'root';
$conf['db_pass'] = 'password';
$conf['db_name'] = 'Limericks';
?>