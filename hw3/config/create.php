﻿<?php

require_once('config.php');
require_once($conf['DOC_ROOT'].'models/entry_model.php');

$mysqli = 
    mysqli_connect($conf['db_host'], $conf['db_user'], $conf['db_pass']);
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') ' . 
        mysqli_connect_error());
}

echo "Successfully connected to MySQL...\n";

$query_create_database = "CREATE DATABASE IF NOT EXISTS {$conf['db_name']};";
if (mysqli_query($mysqli, $query_create_database)) {
    echo "Database {$conf['db_name']} created successfully...\n";
} else {
    echo "Error creating database: " . mysqli_error($mysqli) . '\n';
}
/* Change to $conf['db_name'] database */
mysqli_select_db($mysqli, $conf['db_name']);

$create_tables = <<<EOF
DROP TABLE IF EXISTS Poems;
CREATE TABLE IF NOT EXISTS Poems(PoemNum INT UNSIGNED NOT NULL AUTO_INCREMENT 
    PRIMARY KEY, Title VARCHAR(30) NOT NULL, Author VARCHAR(30) NOT NULL,
    PoemContent VARCHAR(160) NOT NULL, UserRating FLOAT NOT NULL DEFAULT 0,
    YourRating FLOAT, NumRatings INT DEFAULT 0, SubmissionTime INT UNSIGNED,
    FeaturedTimestamp INT UNSIGNED, IsFeatured BOOLEAN DEFAULT FALSE);
EOF;

if (mysqli_multi_query($mysqli, $create_tables)) {
    echo "Successfully created tables...\n";
} else {
    echo "Error creating tables:" . mysqli_error($mysqli) . "\n";
}

/* Get rid of results of query */
while (mysqli_more_results($mysqli) && mysqli_next_result($mysqli));

$string = mysqli_real_escape_string($mysqli, "Hickory, dickory, dock,
The mouse ran up the clock.
The clock struck one,
And down he run,
Hickory, dickory, dock.");
$title = mysqli_real_escape_string($mysqli, "Hickory Dickory Dock");
$author = mysqli_real_escape_string($mysqli, "Unknown");

$query = "INSERT INTO Poems(Title, Author, PoemContent, UserRating, 
    NumRatings, SubmissionTime) VALUES('{$title}', '{$author}', '{$string}', 
    4.5, 2, 1382999300);";
$result = mysqli_query($mysqli, $query);

$string = mysqli_real_escape_string($mysqli, 
"There was an Old Man with a beard
Who said, 'It is just as I feared!
Two Owls and a Hen,
Four Larks and a Wren,
Have all built their nests in my beard!");
$title = mysqli_real_escape_string($mysqli, "Test1");
$author = mysqli_real_escape_string($mysqli, "Edward Lear");

$query = "INSERT INTO Poems(Title, Author, PoemContent, UserRating, 
    NumRatings, SubmissionTime) VALUES('{$title}', '{$author}', '{$string}', 
    4.75, 4, 1382999366);";
$result = mysqli_query($mysqli, $query);

mysqli_close($mysqli);

$string = "Hickory, dickory, dock,
The mouse ran up the clock.
The clock struck one,
And down he run,
Hickory, dickory, dock.";
$title = "Test2";
$author = "You";

$model = new Model();
$tuple = array($title, $author, $string);
$model->submitPoem($tuple);
$title = "Test3";
$tuple = array($title, $author, $string);
$model->submitPoem($tuple);
$title = "Test4";
$tuple = array($title, $author, $string);
$model->submitPoem($tuple);
$title = "Test5";
$tuple = array($title, $author, $string);
$model->submitPoem($tuple);
$title = "Test6";
$tuple = array($title, $author, $string);
$model->submitPoem($tuple);
$title = "Test7";
$tuple = array($title, $author, $string);
$model->submitPoem($tuple);
$title = "Test8";
$tuple = array($title, $author, $string);
$model->submitPoem($tuple);
$title = "Test9";
$tuple = array($title, $author, $string);
$model->submitPoem($tuple);
?>