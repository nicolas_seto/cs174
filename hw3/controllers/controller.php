﻿<?php
/** The controller in the MVC pattern. Processes the user's request.
 *  Authors: Nicolas Seto, Dora Do
 */
interface Controller_{

    /**  Process the request based on request.
     *
     */
    function process();
}
?>