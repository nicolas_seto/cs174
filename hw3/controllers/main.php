﻿<?php
require_once('controller.php');
require_once($conf['DOC_ROOT'].'models/entry_model.php');
require_once($conf['DOC_ROOT'].'views/limerick.php');
require_once($conf['DOC_ROOT'].'views/submission.php');

/** The main controller class for Looney Limericks. Determines whether to
 *  display a featured poem, a specified poem, or submission form based
 *  on the user's request.
 *  Authors: Nicolas Seto, Dora Do
 */
class Controller implements Controller_ {
    private $p;
    private $view;
    private $model;
    private $selected_poem;
    private $topten;
    private $recentten;
    private $viewtype;
    function __construct() {
        /* By default, if no p GET request param is set, retrieve the
           featured poem.
        */
        $this->p = "featured";
        $this->model = new Model();
        $this->viewtype = "limerick";
    }
    
    public function process() {
        if (isset($_GET['rating'])) {
            $rating = $_GET['rating'];
            $this->p = $_GET['p'];
            unset($_GET['rating']);
            
            $this->model->updateRating($this->p, $rating);
        } else {
            /* viewtype can either be limerick or submission */
            if (isset($_GET['v'])) {
                $this->viewtype = $_GET['v'];
            }
            /* p is the unique poem#/identifier. If p is specified, 
             * go to that poem, otherwise, choose a featured poem.
             */
            if (isset($_GET['p'])) {
                $this->p = $_GET['p'];
            }
            /* If the POST variable is set, there was just a submission. */
            if (isset($_POST['limerick'])) {
               $this->selected_poem = array($_POST['title'], $_POST['author'],
                    $_POST['limerick']);
                $this->p = $this->model->submitPoem($this->selected_poem);
                if ($this->p) {
                    /* Gets the id of the poem and gets the poem*/
                    $this->selected_poem = $this->model->getPoem($this->p);
                } else {
                    /* Poem submission was unsuccessful */
                    $this->selected_poem = $this->model->getFeatured();
                }
                /* Clear POST after submission */
                unset($_POST); 
            } else {
                if ($this->p == "featured") {
                    $this->selected_poem = $this->model->getFeatured();
                } else if ($this->p == "random") {
                    $this->selected_poem = $this->model->getRandomPoem();
                } else {
                    $this->selected_poem = $this->model->getPoem($this->p);
                }
            }
            
            $this->topten = $this->model->retrieveTopTenRated();
            $this->recentten = $this->model->retrieveTenRecent();
            
            if ($this->viewtype == "limerick") {
                $this->view = 
                  new mainView($this->selected_poem,
                    $this->topten,$this->recentten);
                $this->view->display();
            } else {
                /* The view is for the submission */
                $this->view = 
                  new submitView($this->topten,$this->recentten);
                $this->view->display();
            }
        }
    }
}
?>