<?php
require_once('view.php');

/** The view in the MVC pattern for displaying limericks.
 *
 */
class mainView implements View_ {
    private $limerick;
    private $topten;
    private $recent;

    function __construct($poem, $topten, $recent) {
        $this->limerick = $poem;
        $this->topten = $topten;
        $this->recent = $recent;
    }
    
    public function display() {
        global $conf;
        $ratings = $this->listRatings();
        $mostrecent = $this->listRecent();
        $poem = $this->listPoem();
        $date = date('Y-m-d H:i:s', $this->limerick['SubmissionTime']);
        $html_code = <<<EOF
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <link rel="stylesheet" type="text/css" 
        href="{$conf['baseURL']}css/style.css" />
        <title>Looney Limericks</title>
        <script src="{$conf['baseURL']}views/scripts/stars.js"></script>
    </head>
<body>
    <div id="topbar">
        <a href="{$conf['baseURL']}index.php?p=random">Random Poem</a>
        &nbsp;
        <a href="{$conf['baseURL']}index.php?v=submission">Upload a Poem</a>
    </div>
    
    <div id="header">
        <h1><a href="{$conf['baseURL']}">Looney Limericks</a></h1>
    </div>
    
    <div id="wrap">
        <div id="left" class="side">
            Highest Rated: <br />
            {$ratings}
        </div>
        <div class="side">
            Recently Submitted: <br />
            {$mostrecent}
        </div>
    </div>
    <div class="content">
        <h4>{$this->limerick['Title']}</h4>
        <i>{$this->limerick['Author']}</i><br />
        <br />
        {$poem}
        <br />
        <br />
        Submitted on {$date}
    </div>
    
    <div id="userrating" class="rate">
        User Rating: <br />
        <script>
            displayUserRating({$this->limerick['UserRating']}, 
                {$this->limerick['PoemNum']});
        </script>
    </div>
    
    <div id="yourrating" class="rate">
        Your Rating: <br />
        <script>
            setYourRating();
        </script>
    </div>
    
</body>
</html>
EOF;
    echo $html_code;
    }
    
    private function listRatings() {
        global $conf;
        $string = "";
        $d = 0;
        for ($i = 0; $i < count($this->topten); $i++) {
            $d = $i + 1;
            $string .= <<<EOD
{$d}. <a href="{$conf['baseURL']}index.php?p={$this->topten[$i]['PoemNum']}">
{$this->topten[$i]['Title']}</a><br />

EOD;
        }
        
        return $string;
    }
    
    private function listRecent() {
        global $conf;
        $string = "";
        $d = 0;
        for ($i = 0; $i < count($this->recent); $i++) {
            $d = $i + 1;
            $string .= <<<EOD
{$d}. <a href="{$conf['baseURL']}index.php?p={$this->recent[$i]['PoemNum']}">
{$this->recent[$i]['Title']}</a><br />

EOD;
        }
        
        return $string;
    }
    
    private function listPoem() {
        global $conf;
        $string = "";
        $lines = explode("\n", $this->limerick['PoemContent']);
        foreach($lines as $line) {
            $string .= $line."<br />";
        }
        return $string;
    }
}

?>