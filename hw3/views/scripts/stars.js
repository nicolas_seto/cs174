var yellow = "<img src=\"views/icons/fullyellow.gif\" alt=\"star\">";
var grey = "<img src=\"views/icons/fullgrey.gif\" alt=\"star\">";
var half = "<img src=\"views/icons/half.gif\" alt=\"star\">";
var count = 0;
var onoff = new Array(false, false, false, false, false);
var pID = 0;
function displayUserRating(rate, p){
    //get the poem ID from the server
    pID = p;
    //get rating from database
    var rounded_rating = Math.round(rate * 2) / 2;
    
    var truncated = Math.floor(rounded_rating);
    
    if(truncated != rounded_rating){
        for(var i = 0; i < (truncated); i++){
            document.write(yellow);
        }
        
        document.write(half);
        
        for(var i = 0; i < (5 - (truncated + 1)); i++){
            document.write(grey);
        }
    } else {
        for(var i = 0; i < rounded_rating; i++){
            document.write(yellow);
        }
        
        for(var i = 0; i < (5 - rounded_rating); i++){
            document.write(grey);
        }
    }
}

function setYourRating(){
    var hoverGrey1 = "<img src=\"views/icons/fullgrey.gif\" " + 
        "id=\"change1\" onClick=\"swapImage1(true, true);\"/>";
    var hoverGrey2 = "<img src=\"views/icons/fullgrey.gif\" " + 
        "id=\"change2\" onClick=\"swapImage2(true, true);\"/>";
    var hoverGrey3 = "<img src=\"views/icons/fullgrey.gif\" " + 
        "id=\"change3\" onClick=\"swapImage3(true, true);\"/>";
    var hoverGrey4 = "<img src=\"views/icons/fullgrey.gif\" " + 
        "id=\"change4\" onClick=\"swapImage4(true, true);\"/>";
    var hoverGrey5 = "<img src=\"views/icons/fullgrey.gif\" " + 
        "id=\"change5\" onClick=\"swapImage5(true, true);\"/>";
    
    document.write(hoverGrey1);
    document.write(hoverGrey2);
    document.write(hoverGrey3);
    document.write(hoverGrey4);
    document.write(hoverGrey5);
    document.write("<br>");
    document.write("<button type=\"button\" " + 
        "onclick=\"submitCount()\">Submit My Rating</button>");
}

function swapImage1(value, value2){
    
    onoff[0] = value;
    
    if (value2) {
        for (var i = 1; i < onoff.length; i++) {
            if (onoff[i]) {
                onoff[i] = false;
                switch(i) {
                    case 1:
                        swapImage2(false, false);
                        break;
                    case 2:
                        swapImage3(false, false);
                        break;
                    case 3:
                        swapImage4(false, false);
                        break;
                    case 4:
                        swapImage5(false, false);
                        break;
                    default:
                        break;
                }
            }
        }
    }
    
    if(onoff[0]){
        document.getElementById("change1").setAttribute('src', 
            'views/icons/fullyellow.gif');
    } else {
        document.getElementById("change1").setAttribute('src', 
            'views/icons/fullgrey.gif');
    }
    
    count = 1;
    
}


function swapImage2(value, value2){
    
    onoff[1] = value;
    
    if (value2) {
        for (var i = 2; i < onoff.length; i++) {
            if (onoff[i]) {
                onoff[i] = false;
                switch(i) {
                    case 2:
                        swapImage3(false, false);
                        break;
                    case 3:
                        swapImage4(false, false);
                        break;
                    case 4:
                        swapImage5(false, false);
                        break;
                    default:
                        break;
                }
            }
        }
        
        if (!onoff[0]) {
            swapImage1(true, false);
        }
    }
    
    if(onoff[1]){
        document.getElementById("change2").setAttribute('src', 
            'views/icons/fullyellow.gif');
    } else {
        document.getElementById("change2").setAttribute('src', 
            'views/icons/fullgrey.gif');
    }
    
    count = 2;
}

function swapImage3(value, value2){
    onoff[2] = value;
    
    if (value2) {
        for (var i = 3; i < onoff.length; i++) {
            if (onoff[i]) {
                onoff[i] = false;
                switch(i) {
                    case 3:
                        swapImage4(false, false);
                        break;
                    case 4:
                        swapImage5(false, false);
                        break;
                    default:
                        break;
                }
            }
        }
        for (var j = 0; j < 2; j++) {
            if (!onoff[j]) {
                onoff[j] = true;
                switch(j) {
                    case 0:
                        swapImage1(true, false);
                        break;
                    case 1:
                        swapImage2(true, false);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    if(onoff[2]){
        document.getElementById("change3").setAttribute('src', 
            'views/icons/fullyellow.gif');
    } else {
        document.getElementById("change3").setAttribute('src', 
            'views/icons/fullgrey.gif');
    }
    
    count = 3;
}

function swapImage4(value, value2){
    onoff[3] = value;
    
    if (value2) {
        if (onoff[4]) {
            onoff[4] = false;
            swapImage5(false, false);
        }
        for (var j = 0; j < 3; j++) {
            if (!onoff[j]) {
                onoff[j] = true;
                switch(j) {
                    case 0:
                        swapImage1(true, false);
                        break;
                    case 1:
                        swapImage2(true, false);
                        break;
                    case 2:
                        swapImage3(true, false);
                    default:
                        break;
                }
            }
        }
    }
    
    if(onoff[3]){
        document.getElementById("change4").setAttribute('src', 
            'views/icons/fullyellow.gif');
    } else {
        document.getElementById("change4").setAttribute('src', 
            'views/icons/fullgrey.gif');
    }
    
    count = 4;
}

function swapImage5(value, value2){
    onoff[4] = value;
    
    if (value2) {
        for (var j = 0; j < 4; j++) {
            if (!onoff[j]) {
                onoff[j] = true;
                switch(j) {
                    case 0:
                        swapImage1(true, false);
                        break;
                    case 1:
                        swapImage2(true, false);
                        break;
                    case 2:
                        swapImage3(true, false);
                        break;
                    case 3:
                        swapImage4(true, false);
                        break;
                    default:
                        break;
                }
            }
        }
    }
    
    if(onoff[4]){
        document.getElementById("change5").setAttribute('src', 
            'views/icons/fullyellow.gif');
    } else {
        document.getElementById("change5").setAttribute('src', 
            'views/icons/fullgrey.gif');
    }
    
    count = 5;
}

//use the count to submit rating to db
function submitCount(){
    alert('Submitted rating: ' + count);
    if (window.XMLHttpRequest) {
        xmlhttp=new XMLHttpRequest();
    } else {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    xmlhttp.open("GET","index.php?rating="+count+
        "&p="+pID,true);
    xmlhttp.send();
}

