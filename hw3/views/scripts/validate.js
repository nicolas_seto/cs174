function displayResult()
{
    var s = document.getElementById("mytextarea").value;
    var n = s.split("\n");
    if (document.getElementById("title").value.length > 30) {
        alert("The title cannot be longer than 30 characters.");
    } else if (document.getElementById("author").value.length > 30) {
        alert("The author's name cannot be longer than 30 characters.");
    } else if (n.length != 5) {
        alert("Error: A limerick must consist of 5 lines.");
    } else {
        for(var i = 0; i < n.length; i++){
            if(n[i].length > 30){
                alert("Each line in the limerick cannot " + 
                    "be longer than 30 characters.");
                break;
            }               
        }
    }
}

function validateText(){
    var s = document.getElementById("mytextarea").value;
    var n = s.split("\n");
    for(var i = 0; i < n.length; i++){
        if(n[i].length > 30){
            return false;
        }               
    }
    if (n.length != 5) {
        return false;
    }
    
    var t = document.getElementsByName("title");
    if(t.length > 30){
        return false;
    }
    
    var a = document.getElementsByName("author");
    if(a.length > 30){
        return false;
    }
        
    return true;
}
    