<?php
require_once('view.php');

/** The view in the MVC pattern for submitting limericks.
 *
 */
class submitView implements View_ {
    private $topten;
    private $recent;
    
    function __construct($topten, $recent) {
        $this->topten = $topten;
        $this->recent = $recent;
    }
    
    public function display() {
        global $conf;
        $ratings = $this->listRatings();
        $mostrecent = $this->listRecent();
        $html_code = <<<EOF
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <link rel="stylesheet" type="text/css" 
        href="{$conf['baseURL']}css/style.css" />
        <title>Looney Limericks</title>
        <script src="{$conf['baseURL']}views/scripts/validate.js"></script>
    </head>
<body>
    <div id="topbar">
        <a href="{$conf['baseURL']}index.php?p=random">Random Poem</a>
        &nbsp;
    </div>
    
    <div id="header">
        <h1><a href="{$conf['baseURL']}">Looney Limericks</a></h1>
    </div>
    
    <div id="wrap">
        <div id="left" class="side">
            Highest Rated: <br />
            {$ratings}
        </div>
        <div class="side">
            Recently Submitted: <br />
            {$mostrecent}
        </div>
    </div>
    
    <div id="upload" class="content">
        <form method="POST" action="{$conf['baseURL']}" 
        onsubmit="return validateText();">
            <br />
            Title: <input type="text" name="title" id="title" maxlength="30"
            placeholder="Limerick Title" />
            <br />
            <br />
            Author: <input type="text" name="author" 
            id="author" maxlength="30"
            placeholder="Author's Name" />
            <br />
            <br />
            <textarea name="limerick" id="mytextarea" rows="6" cols="50" 
            placeholder="Write your poem here."></textarea>
            <br />
            <input type="submit" value="Submit" onclick="displayResult()" />
        </form>
    </div>
</body>
</html>
EOF;
    echo $html_code;
    }
    
    private function listRatings() {
        global $conf;
        $string = "";
        $d = 0;
        for ($i = 0; $i < count($this->topten); $i++) {
            $d = $i + 1;
            $string .= <<<EOD
{$d}. <a href="{$conf['baseURL']}index.php?p={$this->topten[$i]['PoemNum']}">
{$this->topten[$i]['Title']}</a><br />

EOD;
        }
        
        return $string;
    }
    
    private function listRecent() {
        global $conf;
        $string = "";
        $d = 0;
        for ($i = 0; $i < count($this->recent); $i++) {
            $d = $i + 1;
            $string .= <<<EOD
{$d}. <a href="{$conf['baseURL']}index.php?p={$this->recent[$i]['PoemNum']}">
{$this->recent[$i]['Title']}</a><br />

EOD;
        }
        
        return $string;
    }
}
?>