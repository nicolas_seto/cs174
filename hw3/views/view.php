﻿<?php
/** The view in the MVC pattern.
 *  Authors: Nicolas Seto, Dora Do
 */
interface View_ {
    /** Display the given poem along with the top ten and most recent poems.
     *
     */
    function display();
}
?>