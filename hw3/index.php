<?php
session_start();
error_reporting(-1);
/*Authors: Nicolas Seto, Dora Do*/
require_once('config/config.php');

/* By default, controller is main */
$c = 'main';

/* If controller is specified, use it */
if (isset($_GET['c'])) {
    $c = $_GET['c'];
}

/* Include code for specified controller */
require_once 'controllers/' . $c . '.php';

$controller = new Controller();
$controller->process();
?>